//
//  OportunidadesEmAbertoCell.swift
//  Chancce
//
//  Created by Marcos Barbosa on 11/08/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import UIKit

class OportunidadesEmAbertoCell: UITableViewCell {

    
    @IBOutlet weak var numeroOportunidadeAberta: UILabel!
    @IBOutlet weak var tempoLabel: UILabel!
    @IBOutlet weak var dataCanhoto: UILabel!
    
    
    var segundos: Int = 0
    var tempo = Timer()
    var tempoEstaAtivo = false
    
   
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        //segundos = Int(self.tempoLabel.text!)!
        
        iniciandoTempo()
    }
    
    
    
    
    func timeString(time:TimeInterval) -> String {
        let hours = Int(time) / 3600
        let minutes = Int(time) / 60 % 60
        let seconds = Int(time) % 60
        return String(format:"%02i:%02i:%02i", hours, minutes, seconds)
    }
    
    func iniciandoTempo(){
        
        tempo = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(OportunidadesEmAbertoCell.atualizandoTempo)), userInfo: nil, repeats: true)
        
    }
    
    @objc func atualizandoTempo(){
        
        segundos -= 1
        tempoLabel.text = timeString(time: TimeInterval(segundos))
        
        if segundos <= 00 {
            tempo.invalidate()
        }
        
    }
    
    
   
        
}



    
   


