//
//  MeusGanhosCelula.swift
//  Chancce
//
//  Created by Marcos Barbosa on 14/08/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import UIKit

class MeusGanhosCelula: UITableViewCell {

    @IBOutlet weak var numeroMeusGanhos: UILabel!
    @IBOutlet weak var valorMeusGanhos: UILabel!
    @IBOutlet weak var dataMeusGanhos: UILabel!
    @IBOutlet weak var quantidadeMeusGanhos: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
