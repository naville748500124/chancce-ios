//
//  PacoteTableViewCell.swift
//  Chancce
//
//  Created by Marcos Barbosa on 28/08/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import UIKit

class PacoteTableViewCell: UITableViewCell {
    
    @IBOutlet weak var estadoCanhoto: UILabel!
    @IBOutlet weak var dadosCanhoto: UILabel!
    

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
