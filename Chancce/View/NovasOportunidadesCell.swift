//
//  NovasOportunidadesCell.swift
//  Chancce
//
//  Created by Marcos Barbosa on 11/08/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import UIKit

class NovasOportunidadesCell: UITableViewCell {

    @IBOutlet weak var numero: UILabel!
    @IBOutlet weak var valorOportunidade: UILabel!
    @IBOutlet weak var dataOportunidade: UILabel!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
