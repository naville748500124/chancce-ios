//
//  ResumoCelula.swift
//  Chancce
//
//  Created by Marcos Barbosa on 14/08/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import UIKit

class ResumoCelula: UITableViewCell {

    @IBOutlet weak var bloco: UILabel!
    @IBOutlet weak var dataHome: UILabel!
    @IBOutlet weak var descontoHome: UILabel!
    @IBOutlet weak var subTotalHome: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
