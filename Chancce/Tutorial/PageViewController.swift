//
//  PageViewController.swift
//  Chancce
//
//  Created by Marcos Barbosa on 20/09/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import Foundation


import UIKit

class PageViewController: UIPageViewController, UIPageViewControllerDataSource, UIPageViewControllerDelegate {
    
    @IBOutlet weak var botaoCustom: UIButton!
    
    
    lazy var VCArray: [UIViewController] = {
        
        return [self.VCInstance(name: "PrimeiraViewController"), self.VCInstance(name: "SegundaViewController"), self.VCInstance(name: "TerceiraViewController")]
        
    }()
    
    private func VCInstance(name: String) -> UIViewController{
        
        return UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: name)
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataSource = self
        self.delegate = self
        
        if let firstVC = VCArray.first{
            
            setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
            
        }
    }
    
    //mudar cor da status bar
    override public var preferredStatusBarStyle: UIStatusBarStyle{
        
        return .lightContent
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        for view in self.view.subviews{
            if view is UIScrollView{
                view.frame = UIScreen.main.bounds
            }else if view is UIPageControl{
                view.backgroundColor = UIColor.clear
            }
        }
    }
    
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = VCArray.index(of: viewController) else{
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard VCArray.count > previousIndex else{
            return nil
        }
        
        return VCArray[previousIndex]
    }
    
    
    public func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        
        guard let viewControllerIndex = VCArray.index(of: viewController) else{
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        
        guard nextIndex < VCArray.count else {
            return nil
        }
        
        guard VCArray.count > nextIndex else{
            return nil
        }
        
        return VCArray[nextIndex]
        
    }
    
    
    public func presentationCount(for pageViewController: UIPageViewController) -> Int {
        
        return VCArray.count
        
    }
    
    public func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        
        guard let firstViewController = viewControllers?.first, let firstViewControllerIndex = VCArray.index(of: firstViewController) else {
            return 0
        }
        
        return firstViewControllerIndex
    }
    
}
