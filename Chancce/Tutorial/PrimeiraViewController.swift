//
//  PrimeiraViewController.swift
//  Chancce
//
//  Created by Marcos Barbosa on 20/09/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import UIKit

class PrimeiraViewController: UIViewController {
    
    var servidorCompartilhado = UIApplication.shared.delegate as! AppDelegate

    @IBOutlet weak var botaoContinuar: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.botaoContinuar.layer.cornerRadius = 4
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
 
    
    @IBAction func acaoContinuar(_ sender: Any) {
       
        // get parent view controller
        let parentVC = self.parent as! PageViewController
        
        // change page of PageViewController
        parentVC.setViewControllers([parentVC.VCArray[1]], direction: .forward, animated: true, completion: nil)
        
    }

    @IBAction func acaoPular(_ sender: Any) {
        
        
        self.servidorCompartilhado.defaults.set("ja eras", forKey: "validaTutorial")
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil);
        let vc : UIViewController = storyboard.instantiateViewController(withIdentifier: "navigation") as UIViewController;
        self.present(vc, animated: false, completion: nil);
        
        
        
    }
    
    //mudar cor da status bar
    override public var preferredStatusBarStyle: UIStatusBarStyle{
        
        return .lightContent
        
    }

    
}







