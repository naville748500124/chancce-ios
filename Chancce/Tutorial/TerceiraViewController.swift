//
//  TerceiraViewController.swift
//  Chancce
//
//  Created by Marcos Barbosa on 21/09/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import UIKit

class TerceiraViewController: UIViewController {
    
    @IBOutlet weak var botaoVoltar: UIButton!
    @IBOutlet weak var botaoFinalizar: UIButton!
    var servidorCompartilhado = UIApplication.shared.delegate as! AppDelegate

    override func viewDidLoad() {
        super.viewDidLoad()

       
        self.botaoVoltar.layer.cornerRadius = 4
        self.botaoVoltar.layer.borderWidth  = 2
        self.botaoVoltar.layer.backgroundColor = UIColor.clear.cgColor
        self.botaoVoltar.layer.borderColor     = UIColor.white.cgColor
        self.botaoVoltar.setTitleColor(UIColor.white, for: .normal)
        
        self.botaoFinalizar.layer.cornerRadius = 4
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func acaoVoltar(_ sender: Any) {
        
        // get parent view controller
        let parentVC = self.parent as! PageViewController
        
        // change page of PageViewController
        parentVC.setViewControllers([parentVC.VCArray[1]], direction: .reverse, animated: true, completion: nil)

        
    }

    @IBAction func acaoFinalizar(_ sender: Any) {
        
        self.servidorCompartilhado.defaults.set("ja eras", forKey: "validaTutorial")
        let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil);
        let vc : UIViewController = storyboard.instantiateViewController(withIdentifier: "navigation") as UIViewController;
        self.present(vc, animated: false, completion: nil);
        
        
        
    }
    
    //mudar cor da status bar
    override public var preferredStatusBarStyle: UIStatusBarStyle{
        
        return .lightContent
        
    }

}
