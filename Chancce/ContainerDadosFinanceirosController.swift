//
//  ContainerDadosFinanceirosController.swift
//  Chancce
//
//  Created by Marcos Barbosa on 08/08/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import UIKit

class ContainerDadosFinanceirosController: UIViewController {

    //referencia dos campos da tela
    @IBOutlet weak var bancoTextField: UITextField!
    @IBOutlet weak var agenciaTextField: UITextField!
    @IBOutlet weak var contaTextField: UITextField!
    @IBOutlet weak var cpfTextField: UITextField!
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        bancoTextField.layer.cornerRadius   = 5
        bancoTextField.layer.borderColor    = UIColor.gray.cgColor
        bancoTextField.layer.borderWidth    = 0.8
        
        agenciaTextField.layer.cornerRadius = 5
        agenciaTextField.layer.borderColor  = UIColor.gray.cgColor
        agenciaTextField.layer.borderWidth  = 0.8
        
        contaTextField.layer.cornerRadius   = 5
        contaTextField.layer.borderColor    = UIColor.gray.cgColor
        contaTextField.layer.borderWidth    = 0.8
        
        cpfTextField.layer.cornerRadius     = 5
        cpfTextField.layer.borderColor      = UIColor.gray.cgColor
        cpfTextField.layer.borderWidth      = 0.8
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func bancoTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func agenciaTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }

    @IBAction func contaTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func cpfTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
}

extension ContainerDadosFinanceirosController{
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        fecharTeclado()
        
    }
    
    func fecharTeclado(){
        
        self.view.endEditing(true)
        
    }
}
