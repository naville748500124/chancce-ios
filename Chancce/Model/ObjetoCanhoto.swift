//
//  File.swift
//  Chancce
//
//  Created by Marcos Barbosa on 12/09/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import Foundation

class ObjetoCanhoto {
    
    fileprivate var id:     String!
    fileprivate var link:   String!
    fileprivate var data:   String!
    
    init(id: String!, link: String!, data: String!) {
        
        self.id = id
        self.link = link
        self.data = data
        
    }
    
    public func getId() -> String{
        
        return self.id
        
    }
    
    public func getLink() -> String{
        
        return self.link
        
    }
    
    public func getData() ->String{
        
        return self.data
        
    }
}
