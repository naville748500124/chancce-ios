//
//  CadastreViewController.swift
//  Chancce
//
//  Created by Marcos on 03/07/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import Foundation
import UIKit

class CadastreViewController: UIViewController {
    

    @IBOutlet weak var botaoPerfil: UIButton!
    @IBOutlet weak var botaoDadosFinanceiros: UIButton!
    @IBOutlet weak var botaoFinalizar: UIButton!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        //borda dos botoes
        botaoPerfil.layer.borderWidth = 1
        botaoPerfil.layer.borderColor = UIColor.blue.cgColor
        
        botaoDadosFinanceiros.layer.borderWidth = 1
        botaoDadosFinanceiros.layer.borderColor = UIColor.black.cgColor

        botaoFinalizar.layer.borderWidth = 1
        botaoFinalizar.layer.borderColor =  UIColor.blue.cgColor

        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
}

