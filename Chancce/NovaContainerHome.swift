//
//  NovaContainerHome.swift
//  Chancce
//
//  Created by Marcos Barbosa on 10/08/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import UIKit

class NovaContainerHome: UITabBarController {

    override func viewDidLoad() {
        super.viewDidLoad()

        
        let telaHome = UIViewController()
        telaHome.view.backgroundColor = UIColor.blue
        telaHome.tabBarItem.title = "home"
        
        let novasOportunidades = UIViewController()
        novasOportunidades.view.backgroundColor = UIColor.yellow
        novasOportunidades.tabBarItem.title = "nova"
        
        self.viewControllers = [telaHome, novasOportunidades]
        
        novasOportunidades.isBeingPresented
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
