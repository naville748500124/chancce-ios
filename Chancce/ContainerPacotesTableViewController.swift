//
//  ContainerPacotesTableViewController.swift
//  Chancce
//
//  Created by Marcos Barbosa on 28/08/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import UIKit

class ContainerPacotesTableViewController: UIViewController, UITableViewController {

    struct Pacote {
        
        var identificacao: String!
        var estado        : String!
        
    }

    
    @IBOutlet weak var tabelaPacote: UITableView!
    
    
    var list = [Pacote(identificacao: "20170828 01", estado: "Não auditado"),
                Pacote(identificacao: "20170828 02", estado: "Auditado"),
                Pacote(identificacao: "20170828 03", estado: "Não auditado"),
                Pacote(identificacao: "20170828 04", estado: "Não auditado"),
                Pacote(identificacao: "20170828 05", estado: "Auditado")
    ]
    
    override func viewDidLoad() {
        super.viewDidLoad()

        
        self.tabelaPacote.dataSource = self
        self.tabelaPacote.delegate   = self
        
        
        }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return list.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! PacoteTableViewCell
        
        cell.dadosCanhoto.text = list[indexPath.row].identificacao
        cell.estadoCanhoto.text = list[indexPath.row].estado

        

        return cell
    }
    

    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
