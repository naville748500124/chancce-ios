//
//  ConfiguracoesViewController.swift
//  Chancce
//
//  Created by Marcos Barbosa on 10/08/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import UIKit

class ConfiguracoesViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate  {
    
    //referencia dos campos da tela
    @IBOutlet weak var nomeTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var telefoneTextField: UITextField!
    @IBOutlet weak var bancoTextField: UITextField!
    @IBOutlet weak var agenciaTextField: UITextField!
    @IBOutlet weak var contaTextField: UITextField!
    @IBOutlet weak var cpfTextField: UITextField!
    @IBOutlet weak var botaoFinalizar: UIButton!
    @IBOutlet weak var imagemPerfil: UIImageView!
    @IBOutlet weak var botaoAdicionarFoto: UIButton!
    @IBOutlet weak var viewBotoesFoto: UIView!
    @IBOutlet weak var nomeUsuarioTextField: UILabel!
    @IBOutlet weak var botaoMudarSenha: UIButton!
    @IBOutlet weak var digitoTextField: UITextField!
    @IBOutlet weak var viewActivity: UIView!
    @IBOutlet weak var activityConfiguracoes: UIActivityIndicatorView!
    @IBOutlet weak var botaoEstrela: UIButton!
    @IBOutlet weak var bordaTop: UIImageView!
    
    //variaveis
    let acaoFechar =        UIAlertAction(title: "Fechar", style: .default, handler: nil)
    var nome:               String!
    var celular:            String!
    var CPF:                String!
    var email:              String!
    
    var emailRecuperado:    String!
    var validadorEmail:     Bool!
    var banco:              String!
    var agencia:            String!
    var conta:              String!
    var digito:             String!
    var imagemRecuperada:   UIImage!
    var fixaTeclado =       false
    var bancoPicker = UIPickerView()
    var bancoEscolhido: String!
    var servidorCompartilhado = UIApplication.shared.delegate as! AppDelegate

    
    var arrayBancos      = [Banco]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.bordaTop.isHidden = true
        
        //popula picker banco
        
        bancoPicker.delegate = self
        bancoPicker.dataSource = self
        bancoPicker.tag = 1
        bancoTextField.inputView = bancoPicker
        
        
        print(imagemUser)
        
                //let url = URL(string:imagemUser)!
        let url = URL(string:"\(servidorCompartilhado.servidor)upload/foto_usuario/\(imagemUser!)")!
       
        let dado = try? Data(contentsOf: url)
        let imagem: UIImage = UIImage(data: dado!)!
        
       

        
       
        imagemPerfil.image = imagem
        nomeUsuarioTextField.text   = userVizualiza
        nomeTextField.text          = userVizualiza
        telefoneTextField.text      = telefoneUser
        emailTextField.text         = emailUser
        agenciaTextField.text       = agenciaUser
        contaTextField.text         = contaUser
        digitoTextField.text        = digitoUser
        cpfTextField.text           = cpfUser
        
        
        
        //borda dos botoes
        botaoFinalizar.layer.cornerRadius = 8
        botaoMudarSenha.layer.cornerRadius = 8
        
        
        //customizaçao dos campos
        nomeTextField.layer.cornerRadius = 5
        nomeTextField.layer.borderWidth = 0.8
        nomeTextField.layer.borderColor = UIColor.gray.cgColor
        
        emailTextField.layer.cornerRadius = 5
        emailTextField.layer.borderWidth = 0.8
        emailTextField.layer.borderColor = UIColor.gray.cgColor
        
        
        telefoneTextField.layer.cornerRadius = 5
        telefoneTextField.layer.borderWidth = 0.8
        telefoneTextField.layer.borderColor = UIColor.gray.cgColor
        
        
        bancoTextField.layer.cornerRadius   = 5
        bancoTextField.layer.borderColor    = UIColor.gray.cgColor
        bancoTextField.layer.borderWidth    = 0.8
        
        agenciaTextField.layer.cornerRadius = 5
        agenciaTextField.layer.borderColor  = UIColor.gray.cgColor
        agenciaTextField.layer.borderWidth  = 0.8
        
        contaTextField.layer.cornerRadius   = 5
        contaTextField.layer.borderColor    = UIColor.gray.cgColor
        contaTextField.layer.borderWidth    = 0.8
        
        cpfTextField.layer.cornerRadius     = 5
        cpfTextField.layer.borderColor      = UIColor.gray.cgColor
        cpfTextField.layer.borderWidth      = 0.8
        
        digitoTextField.layer.cornerRadius     = 5
        digitoTextField.layer.borderColor      = UIColor.gray.cgColor
        digitoTextField.layer.borderWidth      = 0.8
        
        



        
        
        //arredondar a foto do foto escolhida
        imagemPerfil?.layer.cornerRadius = imagemPerfil.frame.size.width/2
        imagemPerfil?.clipsToBounds = true
        
        //view de escolher galeria ou camera
        viewBotoesFoto.isHidden = true
        viewBotoesFoto.layer.cornerRadius = 5
        
        
        
        if topUser == "1"{
        
            self.bordaTop.isHidden = false
            self.botaoEstrela.setImage(#imageLiteral(resourceName: "invalidName"), for: .normal)
        
        }
        
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
        
        selecionarBanco()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        
        if isInternetAvailable() == true{
            
            self.iniciarAnimacao()
            BuscarBancos()
            contaBadge()
            
        }else{
            
            let alertaUsuario = UIAlertController(title: "Nova Oportunidade", message: "Falha na conexão com a internet.", preferredStyle: .alert)
            let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
            alertaUsuario.addAction(acaoFechar)
            self.present(alertaUsuario, animated: true, completion: nil)
            self.pararAnimacao()
            
        }
        
    }
    
    
    func iniciarAnimacao(){
        
        self.activityConfiguracoes.startAnimating()
        self.viewActivity.isHidden = false
        
    }
    
    func pararAnimacao(){
        
        self.activityConfiguracoes.stopAnimating()
        self.viewActivity.isHidden = true
        
    }

    
    func contaBadge(){
        
        let badge = Int(badgeUser)
        
        if badge! > 0 {
            if (tabBarController?.tabBar.items?[2].badgeValue) != nil {
                print("entou no if badge")
                tabBarController?.tabBar.items?[2].badgeValue = String(describing: "1")
            }else{
                print("entou no else badge")
                tabBarController?.tabBar.items?[2].badgeValue = String(describing: "1")
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    
    @IBAction func nomeTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func emailTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    
    @IBAction func telefoneTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func bancoTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func agenciaTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func contaTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func cpfTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func senhaAtualTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    
    @IBAction func novaSenhaTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func confirmarSenhaTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func digitoTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    
    @IBAction func viewMudarSenha(_ sender: Any) {
        
        fecharTeclado()
        performSegue(withIdentifier: "irAlterarSenha", sender: self)
        
    }
    
    
    
    @IBAction func acaoBotaoCameraOuGaleria(_ sender: Any) {
        
        if self.viewBotoesFoto.isHidden == true{
            
            self.viewBotoesFoto.isHidden = false
            
        }else{
            
            self.viewBotoesFoto.isHidden = true
            
        }
        
        
    }
    
    
    //função do botao que vai abrir a camera
    @IBAction func abrirCamera(_ sender: Any) {
        
        let image = UIImagePickerController()
        image.delegate = self
        
        image.sourceType = UIImagePickerControllerSourceType.camera
        
        
        image.allowsEditing = true
        
        
        
        self.present(image, animated: true){
            
        }
        self.viewBotoesFoto.isHidden = true
        self.pararAnimacao()
        
        
    }
    
    
    //funcao do botao que vai abrir a galeria
    @IBAction func abrirGaleria(_ sender: Any) {
        
        fecharTeclado()
        
        let image = UIImagePickerController()
        image.delegate = self
        
        image.sourceType = UIImagePickerControllerSourceType.photoLibrary
        
        
        image.allowsEditing = true
        
        
        
        self.present(image, animated: true){
            
        }
        self.viewBotoesFoto.isHidden = true
        self.pararAnimacao()
        
    }
    
    
    //funcao do botao finalizar, que irá verificar os dados nos campos e ira validar, se estiver algo errado imprimira uma mensagem, caso contrario irá cadastrar no banco
    @IBAction func acaoBotaoFinalizar(_ sender: Any) {
        
        fecharTeclado()
        CPF = cpfTextField.text
        emailRecuperado = emailTextField.text
        
        if nomeTextField.text == "" {
            
            let alertaUsuario = UIAlertController(title: "Atualização", message: "O campo nome está vazio.", preferredStyle: .alert)
            alertaUsuario.addAction(acaoFechar)
            present(alertaUsuario, animated: true, completion: nil)
            
            
        }else{
            
            self.nome = nomeTextField.text
            
            if emailTextField.text == ""{
                
                let alertaUsuario = UIAlertController(title: "Atualização", message: "O campo email está vazio.", preferredStyle: .alert)
                alertaUsuario.addAction(acaoFechar)
                present(alertaUsuario, animated: true, completion: nil)
                
            }else{
                
                validaEmail()
                
                if validadorEmail ==  false{
                    
                    let alertaUsuario = UIAlertController(title: "Atualização", message: "O email digitado não é valido.", preferredStyle: .alert)
                    alertaUsuario.addAction(acaoFechar)
                    present(alertaUsuario, animated: true, completion: nil)
                    
                }else{
                    
                    self.emailRecuperado = emailTextField.text
                    
                    if telefoneTextField.text == ""{
                        
                        let alertaUsuario = UIAlertController(title: "Atualização", message: "O campo celular está vazio.", preferredStyle: .alert)
                        alertaUsuario.addAction(acaoFechar)
                        present(alertaUsuario, animated: true, completion: nil)
                        
                    }else{
                        
                        self.celular = telefoneTextField.text
                        
                        if telefoneTextField.text! == "" || (telefoneTextField.text?.characters.count)! < 15{
                            
                            let alertaUsuario = UIAlertController(title: "Atualização", message: "O campo celular está vazio, ou está incorreto.", preferredStyle: .alert)
                            alertaUsuario.addAction(acaoFechar)
                            present(alertaUsuario, animated: true, completion: nil)
                            
                            }else{
                                self.agencia = agenciaTextField.text
                                
                                if contaTextField.text == "" || digitoTextField.text == ""{
                                    
                                    let alertaUsuario = UIAlertController(title: "Atualização", message: "O campo conta ou digito está vazio.", preferredStyle: .alert)
                                    alertaUsuario.addAction(acaoFechar)
                                    present(alertaUsuario, animated: true, completion: nil)
                                    
                                }else{
                                    self.conta = contaTextField.text
                                    self.digito = digitoTextField.text
                                    
                                    if cpfTextField.text == ""{
                                        
                                        let alertaUsuario = UIAlertController(title: "Atualização", message: "O campo CPF está vazio.", preferredStyle: .alert)
                                        alertaUsuario.addAction(acaoFechar)
                                        present(alertaUsuario, animated: true, completion: nil)
                                        
                                    }else{
                                        
                                        if CPF.isValidCPF == false {
                                            let alertaUsuario = UIAlertController(title: "Atualização", message: "O CPF digitado é invalido.", preferredStyle: .alert)
                                            alertaUsuario.addAction(acaoFechar)
                                            present(alertaUsuario, animated: true, completion: nil)
                                            
                                            
                                        }else{
                                            
                                            self.CPF = cpfTextField.text
                                            
                                            if imagemPerfil.image == nil {
                                                let alertaUsuario = UIAlertController(title: "Atualização", message: "Nenhuma imagem foi selecionada.", preferredStyle: .alert)
                                                alertaUsuario.addAction(acaoFechar)
                                                present(alertaUsuario, animated: true, completion: nil)
                                                
                                            }else{
                                                
                                                self.iniciarAnimacao()
                                                AtualizarUsuario()
                                                
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }



//somente conexao com webService
extension ConfiguracoesViewController{
    
    
    func AtualizarUsuario(){
        
        //constante abaixo ira receber o caminho do webservice
        guard let url = URL(string: "\(servidorCompartilhado.servidor)Controller_webservice/atualizar_Usuario") else {return}

        
        //variavel de requisicao ira receber o caminho da constante acima
        var request = URLRequest(url:url)
        
        //informa o tipo de envio
        request.httpMethod = "POST"
        
        
        //Use image name from bundle to create NSData
        let image : UIImage = imagemPerfil.image!
        
        let imageData = image.lowestQualityJPEGNSData
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        
        
        
        //constante ira cricar uma string com o id que o webService espera receber e com valor ja associado
        let postString = "nomeUsuario=\(self.nome!)&nomeUsuarioVisualizacao=\(self.nome!)&emailUsuario=\(self.emailRecuperado!)&telefoneUsuario=\(self.celular!)&bancoUsuario=\(self.banco!)&agenciaUsuario=\(self.agencia!)&contaUsuario=\(self.conta!)&cpfUsuario=\(self.CPF!)&imagemUsuario=\(strBase64)&digitoUsuario=\(self.digito!)&idUsuario=\(id!)"
        
//        print(self.nome)
//        print(self.emailRecuperado)
//        print(self.celular)
//        print(self.banco)
//        print(self.agencia)
//        print(self.conta)
//        print(self.CPF)
//        print(self.digito)
//        print(id)
//        print(strBase64)
//        
        
        //abaixo sera feira uma associacao de tipo de codificaao de caracteres
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        
        //constante abaixo sera onde sera feita a comunicacao com o webservice
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            DispatchQueue.main.async() {
                
                //abaixo ira mostrar o estado da conexao com o webservice
                if let response = response {
                    print(response)
                }
                
                if let data = data {
                    
                    print(data)
                    
                    do{
                        
                        //se tudo ocorrer bem, a constante fetchedData ira receber os dados do webService
                        let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, AnyObject>
                        print(json)
                        
                        let validaAtualizacao = json["success"] as! Int
                        
                        
                        if validaAtualizacao == 1 {
                            
                            let alertaUsuario = UIAlertController(title: "Atualizar", message: "O usuario foi atualizado com sucesso.", preferredStyle: .alert)
                            alertaUsuario.addAction(self.acaoFechar)
                            self.present(alertaUsuario, animated: true, completion: nil)
                        
                            
                        }else{
                            
                            let alertaUsuario = UIAlertController(title: "Atualizar", message: "O usuario não pode ser atualizado.", preferredStyle: .alert)
                            alertaUsuario.addAction(self.acaoFechar)
                            self.present(alertaUsuario, animated: true, completion: nil)
                            
                            
                        }

                        self.pararAnimacao()
                        
                    }catch {
                        //se nao conseguir se conectar ao webservice ira mostrar o erro aqui
                        print(error)
                        
                        let alertaUsuario = UIAlertController(title: "Atençao", message: "Ocorreu um erro ao atualizar.", preferredStyle: .alert)
                        let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
                        alertaUsuario.addAction(acaoFechar)
                        self.present(alertaUsuario, animated: true, completion: nil)
                        self.pararAnimacao()
                        
                    }
                }
                
            }}
        task.resume()
    }

    
}



//função que ira recuperar a imagem e atribui-la a variavel imagemPerfil
extension ConfiguracoesViewController{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage{
            
            imagemPerfil.image = image
            
        }else{
            
            print("deu erro na imagem ")
            
        }
        
        self.dismiss(animated: true, completion: nil)
         self.pararAnimacao()
    }
   
}

//função que ira subir alguns campos para o teclado não ficar em cima
extension ConfiguracoesViewController{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if nomeTextField.isEditing == true || emailTextField.isEditing ==  true {
            
            moveTextField(textField: textField, moveDistance: 0, up: false)
            self.fixaTeclado = true
            
        }else {
            
            moveTextField(textField: textField, moveDistance: -180, up: true)
            self.fixaTeclado = false
            
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if self.fixaTeclado == true{
            print("print if")
            moveTextField(textField: textField, moveDistance: 0, up: false)
            
        }else{
            
            print("else")
            moveTextField(textField: textField, moveDistance: -180, up: false)
            
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    
    func moveTextField(textField: UITextField, moveDistance:  Int, up: Bool){
        
        let moveDuration = 0.3
        let movement: CGFloat = CGFloat(up ? moveDistance: -moveDistance)
        
        UIView.beginAnimations("animatedTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(moveDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
        
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
        
    }
    
    //esta funcao ira limitar o numero de caracteres nos campos
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (agenciaTextField.inputDelegate != nil) {
            
            return agenciaTextField.text!.characters.count + string.characters.count <= 8
            
        }else if (contaTextField.inputDelegate != nil){
            
            return contaTextField.text!.characters.count + string.characters.count <= 16
            
        }else if (nomeTextField.inputDelegate != nil){
            
            return nomeTextField.text!.characters.count + string.characters.count <= 96
            
        }else if (emailTextField.inputDelegate != nil){
            
            return emailTextField.text!.characters.count + string.characters.count <= 64
            
        }else if (digitoTextField.inputDelegate != nil){
            
            return digitoTextField.text!.characters.count + string.characters.count <= 1
            
        }
        
        return true
    }
    
}

//função de fechar o teclado
extension ConfiguracoesViewController{
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        fecharTeclado()
        
    }
    
    func fecharTeclado(){
        
        self.view.endEditing(true)
        
    }
}

//função que ira verificar o email
extension ConfiguracoesViewController{
    
    func validaEmail(){
        
        if emailRecuperado.isValidEmail(){
            
            self.validadorEmail = true
            
        }else{
            
            self.validadorEmail = false
            
        }
        
    }
    
}




//função de validar email e validar cpf
extension String {
    var isValidCPF: Bool {
        let numbers = characters.flatMap({Int(String($0))})
        guard numbers.count == 11 && Set(numbers).count != 1 else { return false }
        let soma1 = 11 - ( numbers[0] * 10 +
            numbers[1] * 9 +
            numbers[2] * 8 +
            numbers[3] * 7 +
            numbers[4] * 6 +
            numbers[5] * 5 +
            numbers[6] * 4 +
            numbers[7] * 3 +
            numbers[8] * 2 ) % 11
        let dv1 = soma1 > 9 ? 0 : soma1
        let soma2 = 11 - ( numbers[0] * 11 +
            numbers[1] * 10 +
            numbers[2] * 9 +
            numbers[3] * 8 +
            numbers[4] * 7 +
            numbers[5] * 6 +
            numbers[6] * 5 +
            numbers[7] * 4 +
            numbers[8] * 3 +
            numbers[9] * 2 ) % 11
        let dv2 = soma2 > 9 ? 0 : soma2
        return dv1 == numbers[9] && dv2 == numbers[10]
    }
    
    func isValidEmail() -> Bool {
        
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: characters.count)) != nil
        
    }
    
}

//funcao de popular o picker
extension ConfiguracoesViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayBancos.count
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        return arrayBancos[row].nomeBanco
        
    }
    
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.bancoEscolhido = String(arrayBancos[row].nomeBanco)
        self.banco = String(arrayBancos[row].idBanco)
        bancoTextField.text = arrayBancos[row].nomeBanco
        
    }
    
    func BuscarBancos(){
        
        arrayBancos = []
        
        //constante abaixo ira receber o caminho do webservice
        guard let url = URL(string: "\(servidorCompartilhado.servidor)Controller_webservice/listar_Bancos") else {return}

        
        //variavel de requisicao ira receber o caminho da constante acima
        var request = URLRequest(url:url)
        
        //informa o tipo de envio
        request.httpMethod = "POST"
        
        
        //constante ira cricar uma string com o id que o webService espera receber e com valor ja associado
        let postString = ""
        
        
        //abaixo sera feira uma associacao de tipo de codificaao de caracteres
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        
        //constante abaixo sera onde sera feita a comunicacao com o webservice
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            DispatchQueue.main.async() {
                
                //abaixo ira mostrar o estado da conexao com o webservice
                if let response = response {
                    print(response)
                }
                
                if let data = data {
                    
                    print(data)
                    
                    do{
                        
                        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! NSArray
                        print(json)
                        
                        self.arrayBancos.append(Banco(idBanco: 0, nomeBanco: "Selecione..."))
                        
                        for eachArrayInformacoes in json{
                            
                        
                            
                            let eachInformacoes = eachArrayInformacoes as! [String: Any]
                            let idBanco = eachInformacoes["idBanco"] as? String
                            let nomeBanco = eachInformacoes["nomeBanco"] as? String
                            
                            if idBanco == bancoUser{
                                self.bancoTextField.text = nomeBanco
                                self.banco = idBanco
                            }
                            
                            self.arrayBancos.append(Banco(idBanco: Int(idBanco!)!, nomeBanco: nomeBanco!))
                            
                            
                            
                            print(self.arrayBancos)
                        }
                        
                        self.pararAnimacao()
                        
                    }catch {
                        //se nao conseguir se conectar ao webservice ira mostrar o erro aqui
                        print(error)
                        
                    }
                }
                
            }}
        task.resume()
        
        
    }
    
    func selecionarBanco(){
        
        let tooBar = UIToolbar()
        tooBar.barStyle = UIBarStyle.default
        tooBar.isTranslucent = true
        tooBar.tintColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        tooBar.barTintColor = UIColor(red: 0/255, green: 11/255, blue: 36/255, alpha: 1)
        tooBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Selecionar", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ConfiguracoesViewController.doneBanco))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancelar", style: UIBarButtonItemStyle.plain, target: self, action: #selector(ConfiguracoesViewController.cancelarPicker))
        
        tooBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        tooBar.isUserInteractionEnabled = true
        
        bancoTextField.inputAccessoryView = tooBar
        
    }
    
    //essa funcao escolhe uma banco
    @objc func doneBanco(){
        print("chegou ao done")
        if(self.bancoEscolhido != nil){
            bancoTextField.resignFirstResponder()
        }else{
            bancoTextField.text = self.bancoEscolhido
            bancoPicker.resignFirstResponder()
            selecionarBanco()
            
        }
    }
    
    //essa funcao cancela o escolhimento de uma midia
    @objc func cancelarPicker(){
        
        bancoTextField.resignFirstResponder()
        
    }
    
}


class Banco {
    
    var idBanco: Int
    var nomeBanco: String
    
    init(idBanco: Int, nomeBanco: String){
        
        self.idBanco = idBanco
        self.nomeBanco = nomeBanco
        
    }
    
}


