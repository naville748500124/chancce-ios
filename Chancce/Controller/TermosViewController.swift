//
//  TermosViewController.swift
//  Chancce
//
//  Created by Marcos Barbosa on 19/09/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import UIKit

class TermosViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        let image : UIImage = UIImage(named: "forma1Copiar16.png")!
        let imageView = UIImageView(frame: CGRect(x: 100, y: 100, width: 30, height: 32))
        imageView.contentMode = .scaleAspectFit
        imageView.image = image
        self.navigationItem.titleView = imageView
        
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "< Voltar", style: .plain, target: self, action: nil)
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.clear
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func acaoVoltar(_ sender: Any) {
        
        print("clicou no voltar")
         self.navigationController?.popToRootViewController(animated: true)
        
    }

}
