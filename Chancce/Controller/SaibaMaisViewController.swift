//
//  SaibaMaisViewController.swift
//  Chancce
//
//  Created by Marcos Barbosa on 11/09/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import UIKit

var validaPegou = 0

class SaibaMaisViewController: UIViewController {

    
    @IBOutlet weak var botaoPegarOportunidade: UIButton!
    @IBOutlet weak var botaoCancelar: UIButton!
    @IBOutlet weak var webView: UIWebView!
   
    
     var servidorCompartilhado = UIApplication.shared.delegate as! AppDelegate
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        botaoPegarOportunidade.layer.cornerRadius = 8
        botaoCancelar.layer.cornerRadius = 8
        
      
        
        
        if let url = URL(string: "\(servidorCompartilhado.servidor)tutorial_app.php"){
            let request = URLRequest(url: url)
            self.webView.loadRequest(request)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
    @IBAction func acaoCancelar(_ sender: Any) {
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    
   
    @IBAction func acaoPegar(_ sender: Any) {
        
        
        pegarBloco()
        
        
        
    }
    

}


//somente conexao com webService
extension SaibaMaisViewController{
    
    
    func pegarBloco(){
        
        //constante abaixo ira receber o caminho do webservice
        guard let url = URL(string: "\(servidorCompartilhado.servidor)Controller_webservice/selecionou_Bloco") else {return}
        
        
        //variavel de requisicao ira receber o caminho da constante acima
        var request = URLRequest(url:url)
        
        //informa o tipo de envio
        request.httpMethod = "POST"
        
        
        //constante ira cricar uma string com o id que o webService espera receber e com valor ja associado
        let postString = "id_usuario=\(id!)&id_bloco=\(blocoEscolhido!)"
       
        
        
        
        //abaixo sera feira uma associacao de tipo de codificaao de caracteres
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        
        //constante abaixo sera onde sera feita a comunicacao com o webservice
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            DispatchQueue.main.async() {
                
                //abaixo ira mostrar o estado da conexao com o webservice
                if let response = response {
                    print(response)
                }
                
                if let data = data {
                    
                    print(data)
                    
                    do{
                        
                       
                        
                        //se tudo ocorrer bem, a constante fetchedData ira receber os dados do webService
                        let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, AnyObject>
                        print(json)
                        
                        
                        
                            let badge = json["badge_blocos"] as? String
                        
                            badgeUser = badge
                            
                            
                        
                        
                            self.navigationController?.popViewController(animated: true)
                        
                        
                    }catch {
                        //se nao conseguir se conectar ao webservice ira mostrar o erro aqui
                        print(error)
                        
                    }
                }
                
            }}
        
        
        
        task.resume()
    }
    
    
}
