//
//  OportunidadesEmAbertoViewController.swift
//  Chancce
//
//  Created by Marcos Barbosa on 10/08/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import UIKit


var abreCanhoto = false
var contador = 0


class OportunidadesEmAbertoViewController: UIViewController, UITableViewDelegate, UITableViewDataSource{
    
    
    @IBOutlet weak var tabela: UITableView!
    @IBOutlet weak var viewActivity: UIView!
    @IBOutlet weak var activityOportunidades: UIActivityIndicatorView!
    
    var servidorCompartilhado = UIApplication.shared.delegate as! AppDelegate
    
    struct ListarBlocos {
        
        var idBloco:        String
        var tempoRestante:  String
        var dataBloco:      String
        
    }
    
    var list = [ListarBlocos]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
      
        
        
    }
   
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        if isInternetAvailable() == true{
            
            self.iniciarAnimacao()
            listarBlocos()
            contaBadge()
            
            
            
            
            DispatchQueue.main.async {
                self.listarBlocos()
                self.tabela.reloadData()
                
            }

            
        }else{
            
            let alertaUsuario = UIAlertController(title: "Aviso", message: "Falha na conexão com a internet.", preferredStyle: .alert)
            let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
            alertaUsuario.addAction(acaoFechar)
            self.present(alertaUsuario, animated: true, completion: nil)
            self.pararAnimacao()
            
        }
       
        
    }
    
    
    
    func iniciarAnimacao(){
        
        self.activityOportunidades.startAnimating()
        self.viewActivity.isHidden = false
        
    }
    
    func pararAnimacao(){
        
        self.activityOportunidades.stopAnimating()
        self.viewActivity.isHidden = true
        
    }

    
    func contaBadge(){
        
        
        let badge = Int(badgeUser)
        
        if badge! > 0 {
            if (tabBarController?.tabBar.items?[2].badgeValue) != nil {
                
                print("entou no if badge")
                tabBarController?.tabBar.items?[2].badgeValue = String(describing: "1")
                
            }else{
                
                print("entou no else badge")
                tabBarController?.tabBar.items?[2].badgeValue = String(describing: "1")
                
            }
        }else{
            
            print("entou no else else")
            tabBarController?.tabBar.items?[2].badgeValue = nil
            
        }
        
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        
        return 1
        
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
       
        contador = list.count
        
        
        return contador
        
    }
    
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tabela.dequeueReusableCell(withIdentifier: "celulaOportunidade") as! OportunidadesEmAbertoCell
        
        
        DispatchQueue.main.async {
         
            cell.numeroOportunidadeAberta.text = String(self.list[indexPath.row].idBloco)
            cell.segundos = Int(self.list[indexPath.row].tempoRestante)!
            cell.dataCanhoto.text = String(self.list[indexPath.row].dataBloco)
            
        }
    
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
  
        blocoEscolhido = list[indexPath.row].idBloco
        performSegue(withIdentifier: "irCanhoto", sender: self)
        
    }
  
    
}

extension OportunidadesEmAbertoViewController{
    
    
    func listarBlocos(){
        
        //constante abaixo ira receber o caminho do webservice
        guard let url = URL(string: "\(servidorCompartilhado.servidor)Controller_webservice/listar_Blocos_Usuario") else {return}
        
        
        //variavel de requisicao ira receber o caminho da constante acima
        var request = URLRequest(url:url)
        
        //informa o tipo de envio
        request.httpMethod = "POST"
        
        
        //constante ira cricar uma string com o id que o webService espera receber e com valor ja associado
        let postString = "id_usuario=\(id!)"
        
        
        
        
        //abaixo sera feira uma associacao de tipo de codificaao de caracteres
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        
        //constante abaixo sera onde sera feita a comunicacao com o webservice
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            DispatchQueue.main.async() {
                
                //abaixo ira mostrar o estado da conexao com o webservice
                if let response = response {
                    print(response)
                }
                
                if let data = data {
                    
                    print(data)
                    
                    do{
                        
                        
                        
                        //se tudo ocorrer bem, a constante fetchedData ira receber os dados do webService
                        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! NSArray
                        print(json)
                        
                         self.list = []
                        
                        if json.count == 0 {
                            
                            badgeUser = "0"
                            
                        }
                        
                        for eachArrayInformacoes in json{
                            
                            
                            
                            let eachInformacoes = eachArrayInformacoes as! [String: Any]
                            let idBloco         = eachInformacoes["id_bloco"] as? String
                            let tempoRestante   = eachInformacoes["tempo_restante"] as? String
                            let dataBloco       = eachInformacoes["data_bloco"] as? String
                            
                            self.list.append(ListarBlocos(idBloco: idBloco!, tempoRestante: tempoRestante!, dataBloco: dataBloco!))
                            
                        }
                        
                        self.pararAnimacao()
                        self.contaBadge()

                        
                        self.tabela.reloadData()
                       
                        if  self.list.count == 0 {
                            let alertaUsuario = UIAlertController(title: "Aviso", message: "Você não tem oportunidades em aberto no momento.", preferredStyle: .alert)
                            let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
                            alertaUsuario.addAction(acaoFechar)
                            self.present(alertaUsuario, animated: true, completion: nil)
                        }
                        
                        
                    }catch {
                        //se nao conseguir se conectar ao webservice ira mostrar o erro aqui
                        print(error)
                        let alertaUsuario = UIAlertController(title: "Aviso", message: "Ocorreu um erro ao buscar suas oportunidades em aberto.", preferredStyle: .alert)
                        let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
                        alertaUsuario.addAction(acaoFechar)
                        self.present(alertaUsuario, animated: true, completion: nil)
                        self.pararAnimacao()
                        
                    }
                }
                
            }}
        
        
        
        task.resume()
    }
    
    
}

