//
//  NovasOportunidadesViewController.swift
//  Chancce
//
//  Created by Marcos Barbosa on 10/08/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import UIKit

var passaLista = [NovaOportunidade]()

struct NovaOportunidade {
    
    var id           : String
    var valor        : String
    var data         : String
    
}

 var blocoEscolhido: String!


class NovasOportunidadesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource  {
    
    
    @IBOutlet weak var botaoPegarOportunidade: UIButton!
    @IBOutlet weak var botaoCancelar: UIButton!
    @IBOutlet weak var tabelaNova: UITableView!
    @IBOutlet weak var viewActivity: UIView!
    @IBOutlet weak var activityNovasOportunidades: UIActivityIndicatorView!
  
    
    
    
    var list = [NovaOportunidade]()
    var servidorCompartilhado = UIApplication.shared.delegate as! AppDelegate
    
    var indiceEscolhido: IndexPath = []
    var validaOportunidade = 0
   
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
        
        tabelaNova.dataSource = self
        tabelaNova.delegate   = self
       
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        if isInternetAvailable() == true{
            
            self.iniciarAnimacao()
            self.buscarNovosBlocos()
            
        }else{
            
            let alertaUsuario = UIAlertController(title: "Aviso", message: "Falha na conexão com a internet.", preferredStyle: .alert)
            let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
            alertaUsuario.addAction(acaoFechar)
            self.present(alertaUsuario, animated: true, completion: nil)
            self.pararAnimacao()
            
        }
        
        
        DispatchQueue.main.async {
            
            self.tabelaNova.reloadData()
            
        }
        
       contaBadge()
        
    }
    
    func iniciarAnimacao(){
        
        self.activityNovasOportunidades.startAnimating()
        self.viewActivity.isHidden = false
        
    }
    
    func pararAnimacao(){
        
        self.activityNovasOportunidades.stopAnimating()
        self.viewActivity.isHidden = true
        
    }
    
    func contaBadge(){
        
        let badge = Int(badgeUser)
        
        if badge! > 0 {
            if (tabBarController?.tabBar.items?[2].badgeValue) != nil {
                print("entou no if badge")
                tabBarController?.tabBar.items?[2].badgeValue = String(describing: "1")
            }else{
                print("entou no else badge")
                tabBarController?.tabBar.items?[2].badgeValue = String(describing: "1")
            }
        }
        
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return list.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! NovasOportunidadesCell
        
        cell.numero!.text = String(list[indexPath.row].id)
        cell.valorOportunidade?.text = String(describing: formatCurrency(value: Double(list[indexPath.row].valor)!))
        cell.dataOportunidade?.text  = list[indexPath.row].data
       
        return cell
        
    }
    
       func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        print("printando userBadge \(badgeUser)")
        
        
        if self.validaOportunidade == 0 && badgeUser == "0"{
            
            
            
            func pegarOportunidade(action: UIAlertAction){
                
                
                self.indiceEscolhido = indexPath
                blocoEscolhido = list[indexPath.row].id
                performSegue(withIdentifier: "irSaibaMais", sender: self)
             
                
            }
            
            DispatchQueue.main.async {
                self.pararAnimacao()
                
                let alertaUsuario = UIAlertController(title: "Aviso", message: "Clique em saiba mais, para mais detalhes de como auditar e para pegar este bloco.", preferredStyle: .alert)
                let acaoFechar = UIAlertAction(title: "Fechar", style: .destructive, handler: nil)
                let pegar       = UIAlertAction(title: "Saiba mais", style: .default, handler: pegarOportunidade)
                alertaUsuario.addAction(pegar)
                alertaUsuario.addAction(acaoFechar)
                self.present(alertaUsuario, animated: true, completion: nil)
                
                
                
            }
            
        }else{
            
            let alertaUsuario = UIAlertController(title: "Aviso", message: "Você só pode pegar um bloco de oportunidade por vez.", preferredStyle: .alert)
            let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
            alertaUsuario.addAction(acaoFechar)
            self.present(alertaUsuario, animated: true, completion: nil)
            
        }
        
    }
    
   
}


//somente conexao com webService
extension NovasOportunidadesViewController{
    
    
    func buscarNovosBlocos(){
        
        //constante abaixo ira receber o caminho do webservice
        guard let url = URL(string: "\(servidorCompartilhado.servidor)Controller_webservice/listar_Blocos") else {return}
        
        
        //variavel de requisicao ira receber o caminho da constante acima
        var request = URLRequest(url:url)
        
        //informa o tipo de envio
        request.httpMethod = "POST"
        
        
               //constante ira cricar uma string com o id que o webService espera receber e com valor ja associado
        let postString = "id_usuario=\(id!)"
        print("printando o id")
        print(id)
        
       
        
        //abaixo sera feira uma associacao de tipo de codificaao de caracteres
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        
        //constante abaixo sera onde sera feita a comunicacao com o webservice
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            DispatchQueue.main.async() {
                
                //abaixo ira mostrar o estado da conexao com o webservice
                if let response = response {
                    print(response)
                }
                
                if let data = data {
                    
                    print(data)
                    
                    do{
                        
                        self.list = []
                        
                        //se tudo ocorrer bem, a constante fetchedData ira receber os dados do webService
                        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! NSArray
                        print(json)
                        
                        if json.count == 0 {
                            
                            let alertaUsuario = UIAlertController(title: "Aviso", message: "No momento não há blocos disponíveis.", preferredStyle: .alert)
                            let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
                            alertaUsuario.addAction(acaoFechar)
                            self.present(alertaUsuario, animated: true, completion: nil)
                            
                        }
                        
                        
                        for eachArrayInformacoes in json{
                            
                            let eachInformacoes = eachArrayInformacoes as! [String: Any]
                            let id = eachInformacoes["id_bloco"] as? String
                            let valor = eachInformacoes["valor"] as? String
                            let data = eachInformacoes["data_bloco"] as? String
                            
                            
                            self.list.append(NovaOportunidade(id: id!, valor: valor!, data: data!))
                            
                            
                        }
                        self.tabelaNova.reloadData()
                        self.pararAnimacao()
                    
                    }catch {
                        //se nao conseguir se conectar ao webservice ira mostrar o erro aqui
                        print(error)
                        
                        let alertaUsuario = UIAlertController(title: "Aviso", message: "Ocorreu um erro ao buscar novos blocos.", preferredStyle: .alert)
                        let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
                        alertaUsuario.addAction(acaoFechar)
                        self.present(alertaUsuario, animated: true, completion: nil)
                        self.pararAnimacao()
                        
                    }
                }
                
            }}
        
        
        
        task.resume()
    }
    
    
}
