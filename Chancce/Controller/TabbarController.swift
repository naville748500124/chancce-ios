//
//  TabbarController.swift
//  Chancce
//
//  Created by Marcos Barbosa on 10/08/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import UIKit

class TabbarController: UITabBarController {

    
    var servidorCompartilhado = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //adiciona imagem na navigation controller
        let image : UIImage = UIImage(named: "forma1Copiar16.png")!
        let imageView = UIImageView(frame: CGRect(x: 100, y: 100, width: 30, height:28))
        imageView.contentMode = .scaleAspectFit
        imageView.image = image
        self.navigationItem.titleView = imageView
        
       
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
  
    @IBAction func botaoSair(_ sender: Any) {
        
        validaSair()
    }
    
    func validaSair(){
        
        let alertaUsuario = UIAlertController(title: "Sair", message: "Você realmente deseja sair?", preferredStyle: .alert)
        let acaoNao = UIAlertAction(title: "Não", style: .default, handler: nil)
        let acaoSim = UIAlertAction(title: "Sim", style: .destructive, handler: saindo)
        alertaUsuario.addAction(acaoNao)
        alertaUsuario.addAction(acaoSim)
        self.present(alertaUsuario, animated: true, completion: nil)
        
        
    }
    
    func saindo(action: UIAlertAction){
        
       
        self.servidorCompartilhado.defaults.set(false, forKey: "validaManterConectado")
        self.servidorCompartilhado.defaults.set("", forKey: "emailDefault")
        self.servidorCompartilhado.defaults.set("", forKey: "senhaDefault")
        
        self.navigationController?.popToRootViewController(animated: true)
        
    }
    

}





