//
//  MeusGanhosViewController.swift
//  Chancce
//
//  Created by Marcos Barbosa on 10/08/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import UIKit



class MeusGanhosViewController: UIViewController, UITableViewDataSource, UITableViewDelegate{

    @IBOutlet weak var tabelaGanhos: UITableView!
    @IBOutlet weak var totalEmAuditacao: UILabel!
    @IBOutlet weak var viewActivity: UIView!
    @IBOutlet weak var activityGanhos: UIActivityIndicatorView!
    
    struct EmAuditacao {
        
        var idBloco             : String
        var dataBloco           : String
        var qtdCanhotos         : String
        var valor               : Double
        var auditados           : String
        
    }
    
    var servidorCompartilhado = UIApplication.shared.delegate as! AppDelegate
    
    var list = [EmAuditacao]()
    var total : Double = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tabelaGanhos.delegate = self
        self.tabelaGanhos.dataSource = self

    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
      
        if isInternetAvailable() == true{
            
            self.total = 0.0
            self.iniciarAnimacao()
            contaBadge()
            self.buscarCanhotosEmAuditacao()
            
        }else{
            
            let alertaUsuario = UIAlertController(title: "Aviso", message: "Falha na conexão com a internet.", preferredStyle: .alert)
            let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
            alertaUsuario.addAction(acaoFechar)
            self.present(alertaUsuario, animated: true, completion: nil)
            self.pararAnimacao()
            
        }
        
        
    }
    
    func iniciarAnimacao(){
        
        self.activityGanhos.startAnimating()
        self.viewActivity.isHidden = false
        
    }
    
    func pararAnimacao(){
        
        self.activityGanhos.stopAnimating()
        self.viewActivity.isHidden = true
        
    }
    
    func contaBadge(){
        
        let badge = Int(badgeUser)
        
        if badge! > 0 {
            if (tabBarController?.tabBar.items?[2].badgeValue) != nil {
                print("entou no if badge")
                tabBarController?.tabBar.items?[2].badgeValue = String(describing: "1")
            }else{
                print("entou no else badge")
                tabBarController?.tabBar.items?[2].badgeValue = String(describing: "1")
            }
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
       
        return list.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! MeusGanhosCelula
        
        cell.numeroMeusGanhos.text     = list[indexPath.row].idBloco
        cell.dataMeusGanhos.text       = ("Data do bloco \(list[indexPath.row].dataBloco)")
        cell.quantidadeMeusGanhos.text = ("Foram auditados \(list[indexPath.row].auditados) de um total de \(list[indexPath.row].qtdCanhotos) canhotos.")
        cell.valorMeusGanhos.text      = String(formatCurrency(value: Double(list[indexPath.row].valor)))
        
       
        
        return cell
        
    }
  
}

extension NumberFormatter {
    convenience init(style: Style) {
        self.init()
        numberStyle = style
    }
}


extension Formatter {
    
       static let currencyBR: NumberFormatter = {
        let formatter = NumberFormatter(style: . currency)
        formatter.locale = Locale(identifier: "pt_BR")
        return formatter
    }()
    
}


extension FloatingPoint {
    
    var currencyBR: String {
        return Formatter.currencyBR.string(for: self) ?? ""
    }
    
}

//somente conexao com webService
extension MeusGanhosViewController{
    
    
    func buscarCanhotosEmAuditacao(){
        
        //constante abaixo ira receber o caminho do webservice
        guard let url = URL(string: "\(servidorCompartilhado.servidor)Controller_webservice/bloco_auditado") else {return}
        
        
        //variavel de requisicao ira receber o caminho da constante acima
        var request = URLRequest(url:url)
        
        //informa o tipo de envio
        request.httpMethod = "POST"
        
        
        //constante ira cricar uma string com o id que o webService espera receber e com valor ja associado
        let postString = "id_usuario=\(id!)"
        
        
        
        //abaixo sera feira uma associacao de tipo de codificaao de caracteres
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        
        //constante abaixo sera onde sera feita a comunicacao com o webservice
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            DispatchQueue.main.async() {
                
                //abaixo ira mostrar o estado da conexao com o webservice
                if let response = response {
                    print(response)
                }
                
                if let data = data {
                    
                    print(data)
                    
                    do{
                        
                        self.list = []
                        
                        //se tudo ocorrer bem, a constante fetchedData ira receber os dados do webService
                        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! NSArray
                        print(json)
                        
                        
                        if json.count == 0 {
                            
                            let alertaUsuario = UIAlertController(title: "Aviso", message: "Não há no momento blocos aguardando auditoria.", preferredStyle: .alert)
                            let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
                            alertaUsuario.addAction(acaoFechar)
                            self.present(alertaUsuario, animated: true, completion: nil)
                            
                        }
                        
                        
                        for eachArrayInformacoes in json{
                            
                            let eachInformacoes = eachArrayInformacoes as! [String: Any]
                            let idBloco = eachInformacoes["id_bloco"] as? String
                            let valor = eachInformacoes["valor"] as? String
                            let dataBloco = eachInformacoes["data_bloco"] as? String
                            let auditados = eachInformacoes["auditados"] as? String
                            let qtdCanhotos = eachInformacoes["qtd_canhotos"] as? String
                            
                            let valorDouble = Double(valor!)
                            
                            self.list.append(EmAuditacao(idBloco: idBloco!, dataBloco: dataBloco!, qtdCanhotos: qtdCanhotos!, valor: valorDouble!, auditados: auditados!))
                            
                            self.total = self.total + valorDouble!
                            
                        }
                        
                        print("Printando o valor de total \(self.total))")
                        self.totalEmAuditacao.text = String(formatCurrency(value: Double(self.total)))
                        
                        self.tabelaGanhos.reloadData()
                        self.pararAnimacao()
                        
                    }catch {
                        //se nao conseguir se conectar ao webservice ira mostrar o erro aqui
                        print(error)
                        
                        let alertaUsuario = UIAlertController(title: "Aviso", message: "Ocorreu um erro ao buscar blocos aguardando auditoria.", preferredStyle: .alert)
                        let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
                        alertaUsuario.addAction(acaoFechar)
                        self.present(alertaUsuario, animated: true, completion: nil)
                        
                    }
                }
            }
        }
       
        task.resume()
        
    }
    
    
}

