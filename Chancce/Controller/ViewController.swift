//
//  ViewController.swift
//  Chancce
//
//  Created by Marcos Barbosa on 30/06/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import UIKit
import CryptoSwift
import MessageUI
import FBSDKLoginKit

var id:             String!
var user:           String!
var userVizualiza:  String!
var emailUser:      String!
var telefoneUser:   String!
var bancoUser:      String!
var agenciaUser:    String!
var contaUser:      String!
var digitoUser:     String!
var cpfUser:        String!
var imagemUser:     String!
var badgeUser:      String!
var topUser:        String!

class ViewController: UIViewController{

    
    @IBOutlet weak var usuarioText: UITextField!
    @IBOutlet weak var senhaText: UITextField!
    @IBOutlet weak var botaoEntrarButton: UIButton!
    @IBOutlet weak var botaoEsqueceuSenha: UIButton!
    @IBOutlet weak var botaoCadastre: UIButton!
    @IBOutlet weak var viewActivity: UIView!
    @IBOutlet weak var activityLogin: UIActivityIndicatorView!
    @IBOutlet weak var manterConectadoSwitch: UISwitch!
    
    @IBOutlet weak var emailLabel: UILabel!
    
    @IBOutlet weak var bnt_Facebook: UIButton!
    
    
    
    let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
    var senhaCriptografada: String!
    var senha: String!
    var email: String!
    var validaUsuario = false
    var servidorCompartilhado = UIApplication.shared.delegate as! AppDelegate
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        
        if servidorCompartilhado.defaults.string(forKey: "validaTutorial") == nil {
            
            servidorCompartilhado.irParaTutorial()
            
        }
        
        self.manterConectadoSwitch.isOn = false
        
        let enviarEmail = UITapGestureRecognizer(target: self, action: #selector(abrirEmail))
        
        emailLabel.addGestureRecognizer(enviarEmail)
        emailLabel.isUserInteractionEnabled = true
        
        self.viewActivity.isHidden = true
        botaoEntrarButton.layer.cornerRadius = 20
        bnt_Facebook.layer.cornerRadius = 20
        botaoCadastre.layer.cornerRadius     = 20
        
         print(servidorCompartilhado.defaults.string(forKey: "validaManterConectado") ?? "")
        
        if servidorCompartilhado.defaults.bool(forKey: "validaManterConectado") == true{
           
            self.manterConectadoSwitch.isOn = true
            
            usuarioText.text = servidorCompartilhado.defaults.string(forKey: "emailDefault")
            senhaText.text   = servidorCompartilhado.defaults.string(forKey: "senhaDefault")
            
            if self.usuarioText.text != "" && self.senhaText.text != ""{
                
                    acaoBotaoEntrar((Any).self)
                
            }
            
            if (servidorCompartilhado.defaults.string(forKey: "facebookDefault") != "") && (servidorCompartilhado.defaults.string(forKey: "facebookDefault") != nil){
                print("Auto Login Facebook")
                
                servidorCompartilhado.id_facebook = servidorCompartilhado.defaults.string(forKey: "facebookDefault")!
                self.validarAcessoFacebook()
                
            }

            
        }else{
            
            print("else")
            print(servidorCompartilhado.defaults.string(forKey: "emailDefault") ?? "")
            print(servidorCompartilhado.defaults.string(forKey: "senhaDefault") ?? "")

            
            manterConectadoSwitch.isOn = false
            self.usuarioText.text = ""
            self.senhaText.text   = ""
            
        }
        
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        self.navigationController?.navigationBar.isHidden = false
    }


    @IBAction func usuarioTextReturn(_ sender: Any) {
        fecharTeclado()
    }

    @IBAction func senhaTextReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func acaoManterConectado(_ sender: Any) {
        
        fecharTeclado()
        
        if self.manterConectadoSwitch.isOn
        {
            self.manterConectadoSwitch.isOn = false
        }
        else
        {
            self.manterConectadoSwitch.isOn = true
        }
        
//        if manterConectadoSwitch.isOn ==  true {
//
//            self.servidorCompartilhado.defaults.set(true, forKey: "validaManterConectado")
//
//        }else{
//
//            self.usuarioText.text = ""
//            self.senhaText.text   = ""
//
//        }
        
    }
   
    @IBAction func acaoBotaoEntrar(_ sender: Any) {
        
        self.activityLogin.startAnimating()
        self.viewActivity.isHidden = false
        
        
        if isInternetAvailable() == true{
            
            Login()
        }else{
            
            let alertaUsuario = UIAlertController(title: "Login", message: "Falha na conexão com a internet.", preferredStyle: .alert)
            alertaUsuario.addAction(self.acaoFechar)
            self.present(alertaUsuario, animated: true, completion: nil)
            self.activityLogin.stopAnimating()
            self.viewActivity.isHidden = true
            
        }
        
        if manterConectadoSwitch.isOn{
            
            self.servidorCompartilhado.defaults.set(true, forKey: "validaManterConectado")
            
            
        }else{
            
            self.servidorCompartilhado.defaults.removeObject(forKey: "validaManterConectado")
            self.usuarioText.text = ""
            self.senhaText.text   = ""
            
        }

       
        
    }
    @IBAction func irEsqueceuSenha(_ sender: UIButton) {
        self.performSegue(withIdentifier: "segueEsqueceuSenha", sender: nil)
    }
    
    @IBAction func irCadastro(_ sender: UIButton){
        self.performSegue(withIdentifier: "irCadastro", sender: nil)
    }
    
    @IBAction func login_Facebook(_ sender: Any) {
        
        self.viewActivity.isHidden = false
        self.activityLogin.isHidden = false
        self.activityLogin.startAnimating()

        print("Login Facebook")
        FBSDKLoginManager().logIn(withReadPermissions: ["email", "public_profile"], from: self){
            (result,err) in
            if err != nil {
                print("Falha no Login")
                return
            }
            
            print("Passou Falha Login")
            
            if !(result?.isCancelled)! {
                print("Processo não foi cancelado")
                print("Token: \(String(describing: result?.token.tokenString!))")
                self.dadosFacebook()
            } else {
                print("Login cancelado")
                self.viewActivity.isHidden = true
                return
            }
            
        }
        
    }
    
    //Com botão do facebook original
    func loginButtonDidLogOut(_ loginButton: FBSDKLoginButton!) {
        print("Logout Facebook")
    }
    
    func loginButton(_ loginButton: FBSDKLoginButton!, didCompleteWith result: FBSDKLoginManagerLoginResult!, error: Error!) {
        
        if error != nil {
            print("didCompleteWith Erro: \(error)")
            return
        }
        
        print("Login realizado.")
    }
    
    func dadosFacebook() {
        print("dadosFacebook")
        FBSDKGraphRequest(graphPath: "/me", parameters: ["fields": "id, name, email, link"]).start{(connection, result, err) in
            
            if err != nil {
                print("Erro ao buscar dados")
                print(err ?? "")
                return
            }
            
            let face:[String:AnyObject] = result as! [String : AnyObject]
            
            self.servidorCompartilhado.id_facebook = face["id"]! as! String
            self.servidorCompartilhado.nome_facebook = face["name"]! as! String
            self.servidorCompartilhado.link_facebook = face["link"]! as! String
            self.servidorCompartilhado.email_facebook = face["email"]! as! String
            
            print("\(self.servidorCompartilhado.id_facebook) ID Facebook")
            print("\(self.servidorCompartilhado.nome_facebook) Nome Facebook")
            print("\(self.servidorCompartilhado.link_facebook) Link Facebook")
            print("\(self.servidorCompartilhado.email_facebook) E-mail Facebook")
            
            self.validarAcessoFacebook()
            print("http://graph.facebook.com/\(self.servidorCompartilhado.id_facebook)/picture?type=large")
            //self.entrar(id_facebook: self.compartilhado.id_facebook, email: "", senha: "")
            
        }
    }
    
    func validarAcessoFacebook() {
        print("validarAcessoFacebook")
        //constante abaixo ira receber o caminho do webservice
        guard let url = URL(string: "\(self.servidorCompartilhado.servidor)Controller_webservice/login_facebook") else {return}
        print(url)
        //variavel de requisicao ira receber o caminho da constante acima
        var request = URLRequest(url:url)
        
        //informa o tipo de envio
        request.httpMethod = "POST"
        //constante ira cricar uma string com o id que o webService espera receber e com valor ja associado
        let postString = "id_facebook=\(self.servidorCompartilhado.id_facebook)"
        print(postString)
        
        //abaixo sera feira uma associacao de tipo de codificaao de caracteres
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        //constante abaixo sera onde sera feita a comunicacao com o webservice
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            DispatchQueue.main.async() {
                print("DispatchQueue Verificar Facebook")
                
                self.viewActivity.isHidden = false
                self.activityLogin.startAnimating()
                
                //abaixo ira mostrar o estado da conexao com o webservice
                if let response = response {
                    print(response)
                }
                
                if let data = data {
                    
                    print(data)
                    
                    do{
                        
                        //se tudo ocorrer bem, a constante fetchedData ira receber os dados do webService
                        let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, AnyObject>
                        
                        print(json)
                        
                        let status          = json["status"] as? Int
                        
                        if status == -1 { //Caso o usuário seja novo
                            print("Usuário novo")
//                            let storyboard : UIStoryboard = UIStoryboard(name: "Main", bundle: nil);
//                            let vc : UIViewController = storyboard.instantiateViewController(withIdentifier: "CadastroViewController") as UIViewController;
//                            self.present(vc, animated: false, completion: nil);
//
                            self.performSegue(withIdentifier: "irCadastro", sender: self)
                        } else { //caso já exista
                            print("Usuário já existe")
                            
                            let userId          = json["idUsuario"] as? Int

                            if  userId ==  0 {
                                
                                let alertaUsuario = UIAlertController(title: "Login", message: "E-mail ou senha inválido", preferredStyle: .alert)
                                alertaUsuario.addAction(self.acaoFechar)
                                self.present(alertaUsuario, animated: true, completion: nil)
                                
                            }else{
                                
                                print("else após json")
                                
                                id             = json["idUsuario"]               as? String
                                user           = json["nomeUsuario"]             as? String
                                userVizualiza  = json["visualizacaoNomeUsuario"] as? String
                                emailUser      = json["emailUsuario"]            as? String
                                telefoneUser   = json["telefoneUsuario"]         as? String
                                bancoUser      = json["bancoUsuario"]            as? String
                                contaUser      = json["contaUsuario"]            as? String
                                agenciaUser    = json["agenciaUsuario"]          as? String
                                cpfUser        = json["cpfUsuario"]              as? String
                                imagemUser     = json["imagemUsuario"]           as? String
                                digitoUser     = json["digitoUsuario"]           as? String
                                badgeUser      = json["badge_blocos"]            as? String
                                topUser        = json["top"]                     as? String
                                
                                if self.manterConectadoSwitch.isOn == true {
                                    
                                    self.servidorCompartilhado.defaults.set(true, forKey: "validaManterConectado")
                                    self.servidorCompartilhado.defaults.set(self.servidorCompartilhado.id_facebook, forKey: "facebookDefault")
                                    
                                }else{
                                    
                                    self.servidorCompartilhado.defaults.set(false, forKey: "validaManterConectado")
                                    
                                    self.servidorCompartilhado.defaults.set("", forKey: "facebookDefault")
                                    self.servidorCompartilhado.defaults.set("", forKey: "emailDefault")
                                    self.servidorCompartilhado.defaults.set("", forKey: "senhaDefault")
                                    
                                }
                                
                                self.performSegue(withIdentifier: "loginSegue", sender: self)
                                
                            }
                            
                            
                        }
                        
                        
                    }catch {
                        //se nao conseguir se conectar ao webservice ira mostrar o erro aqui
                        print(error)
                        print("Catch")
                        let alertaUsuario = UIAlertController(title: "Aviso", message: "Algo deu errado no login.", preferredStyle: .alert)
                        let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
                        alertaUsuario.addAction(acaoFechar)
                        self.present(alertaUsuario, animated: true, completion: nil)
                        self.viewActivity.isHidden = true
                        self.activityLogin.stopAnimating()
                        
                    }
                }
                
            }}
        task.resume()
        
    }
    
    
    
}

extension ViewController {
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        fecharTeclado()
    }
    
    func fecharTeclado(){
        
        self.view.endEditing(true)
        
    }
    
}

extension ViewController{
    
    func Login(){
        
        
        self.email = usuarioText.text?.lowercased()
        self.senha = senhaText.text
         self.senhaCriptografada = senha.sha1()
        
        //constante abaixo ira receber o caminho do webservice
        guard let url = URL(string: "\(servidorCompartilhado.servidor)Controller_webservice/loginApp") else {return}
        
        //variavel de requisicao ira receber o caminho da constante acima
        var request = URLRequest(url:url)
        
        //informa o tipo de envio
        request.httpMethod = "POST"
        
        self.senhaCriptografada = self.senha.sha1()
        
        
        //constante ira cricar uma string com o id que o webService espera receber e com valor ja associado
        let postString = "emailUsuario=\(self.email!)&senhaUsuario=\(self.senhaCriptografada!)"
        
        print(postString)
        
        //abaixo sera feira uma associacao de tipo de codificaao de caracteres
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        
        //constante abaixo sera onde sera feita a comunicacao com o webservice
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            DispatchQueue.main.async() {
                
                //abaixo ira mostrar o estado da conexao com o webservice
                if let response = response {
                    print(response)
                }
                
                if let data = data {
                    
                    print(data)
                    
                    do{
                        
                        //se tudo ocorrer bem, a constante fetchedData ira receber os dados do webService
                        let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, AnyObject>
                        
                        print(json)
                        
                        let userId = json["idUsuario"] as? Int
                        
                        
                        
                        if  userId ==  0 {
                            
                            let alertaUsuario = UIAlertController(title: "Login", message: "E-mail ou senha inválido", preferredStyle: .alert)
                            alertaUsuario.addAction(self.acaoFechar)
                            self.present(alertaUsuario, animated: true, completion: nil)
                            
                        }else{
                            
                            id             = json["idUsuario"]               as? String
                            user           = json["nomeUsuario"]             as? String
                            userVizualiza  = json["visualizacaoNomeUsuario"] as? String
                            emailUser      = json["emailUsuario"]            as? String
                            telefoneUser   = json["telefoneUsuario"]         as? String
                            bancoUser      = json["bancoUsuario"]            as? String
                            contaUser      = json["contaUsuario"]            as? String
                            agenciaUser    = json["agenciaUsuario"]          as? String
                            cpfUser        = json["cpfUsuario"]              as? String
                            imagemUser     = json["imagemUsuario"]           as? String
                            digitoUser     = json["digitoUsuario"]           as? String
                            badgeUser      = json["badge_blocos"]            as? String
                            topUser        = json["top"]                     as? String
                            
                            
                            
                            
                            if self.manterConectadoSwitch.isOn == true {
                                
                                self.servidorCompartilhado.defaults.set(true, forKey: "validaManterConectado")
                                self.servidorCompartilhado.defaults.set(emailUser, forKey: "emailDefault")
                                self.servidorCompartilhado.defaults.set(self.senha, forKey: "senhaDefault")
                                
                            }else{
                                
                                self.servidorCompartilhado.defaults.set(false, forKey: "validaManterConectado")
                                self.servidorCompartilhado.defaults.set("", forKey: "facebookDefault")
                                self.servidorCompartilhado.defaults.set("", forKey: "emailDefault")
                                self.servidorCompartilhado.defaults.set("", forKey: "senhaDefault")
                                
                            }
                            
                            self.performSegue(withIdentifier: "loginSegue", sender: self)
                            
                        }
                        
                        self.viewActivity.isHidden = true
                        self.activityLogin.stopAnimating()
                       
                        
                    }catch {
                        //se nao conseguir se conectar ao webservice ira mostrar o erro aqui
                        print(error)
                        
                        let alertaUsuario = UIAlertController(title: "Aviso", message: "Algo deu errado no login.", preferredStyle: .alert)
                        let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
                        alertaUsuario.addAction(acaoFechar)
                        self.present(alertaUsuario, animated: true, completion: nil)
                        self.viewActivity.isHidden = true
                        self.activityLogin.stopAnimating()
                        
                    }
                }
                
            }}
        task.resume()
    }


 
        
    

}

extension ViewController: MFMailComposeViewControllerDelegate
{
    @objc func abrirEmail() {
        //        if MFMailComposeViewController.canSendMail() {
        //            let mail = MFMailComposeViewController()
        //            mail.mailComposeDelegate = self
        //            mail.setToRecipients(["contato@chancce.com.br"])
        //            mail.setSubject("Contato Chancce")
        //            present(mail, animated: true)
        
        let mail = MFMailComposeViewController()
        
        if MFMailComposeViewController.canSendMail(){
        
            mail.mailComposeDelegate = self
            mail.setToRecipients(["contato@chancce.com.br"])
            mail.setSubject("Contato Chancce")
            mail.setMessageBody("Escreva aqui o motivo de seu contato", isHTML: false)
            present(mail, animated: true, completion: nil)
        
        }
        
    }
    
   
    func mailComposeController(_ controller: MFMailComposeViewController, didFinishWith result: MFMailComposeResult, error: Error?) {
        
        switch result.rawValue {
        case MFMailComposeResult.cancelled.rawValue :
            print("E-mail Cancelado")
            
        case MFMailComposeResult.failed.rawValue :
            print("Falha no E-mail")
            
        case MFMailComposeResult.saved.rawValue :
            print("E-mail Salvo")
            
        case MFMailComposeResult.sent.rawValue :
            print("E-mail enviado")
            
        default: break
        }
        
        dismiss(animated: true, completion: nil)
        
    }
    
    
}
