//
//  HomeViewController.swift
//  Chancce
//
//  Created by Marcos Barbosa on 10/08/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var nomeUserHome: UILabel!
    @IBOutlet weak var viewActivity: UIView!
    @IBOutlet weak var activityHome: UIActivityIndicatorView!
    @IBOutlet weak var tabelaHome: UITableView!
    @IBOutlet weak var descontoTotalHome: UILabel!
    @IBOutlet weak var valorTotalHome: UILabel!
    @IBOutlet weak var imagemHome: UIImageView!
    @IBOutlet weak var imagemEstrela: UIImageView!
    @IBOutlet weak var imagemBorda: UIImageView!
   
    
    
    struct PosAuditacao {
        
        var idBloco             : String
        var dataBloco           : String
        var qtdCanhotos         : String
        var valor               : String
        var auditados           : String
        var posAuditados        : String
        var valorPosAuditados   : String
        var valorDesconto       : String
        
    }
    
    var servidorCompartilhado = UIApplication.shared.delegate as! AppDelegate
    var list = [PosAuditacao]()
    var totalDesconto = 0.0
    var totalReceber  = 0.0

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imagemEstrela.isHidden = true
        self.imagemBorda.isHidden   = true

        self.tabelaHome.delegate = self
        self.tabelaHome.dataSource = self
        
        nomeUserHome.text = "Olá, \(userVizualiza!)"
        
        let url = URL(string:"\(servidorCompartilhado.servidor)upload/foto_usuario/\(imagemUser!)")!
        
        let dado = try? Data(contentsOf: url)
        let imagem: UIImage = UIImage(data: dado!)!
      
        self.imagemHome.image = imagem
        
        //arredondar a foto do foto escolhida
        imagemHome?.layer.cornerRadius = imagemHome.frame.size.width/2
        imagemHome?.clipsToBounds = true
        
        if topUser == "1"{
        
            self.imagemEstrela.isHidden = false
            self.imagemBorda.isHidden   = false
            
        }
        

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        if isInternetAvailable() == true{
            
            self.iniciarAnimacao()
            listarBlocosAuditados()
            contaBadge()
          
            
        }else{
            
            let alertaUsuario = UIAlertController(title: "Aviso", message: "Falha na conexão com a internet.", preferredStyle: .alert)
            let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
            alertaUsuario.addAction(acaoFechar)
            self.present(alertaUsuario, animated: true, completion: nil)
            self.pararAnimacao()
            
        }
        
    }
    
    func iniciarAnimacao(){
        
        self.activityHome.startAnimating()
        self.viewActivity.isHidden = false
        
    }
    
    func pararAnimacao(){
        
        self.activityHome.stopAnimating()
        self.viewActivity.isHidden = true
        
    }

    
    func contaBadge(){
        
        let badge = Int(badgeUser)
        
        if badge! > 0 {
            if (tabBarController?.tabBar.items?[2].badgeValue) != nil {
                print("entou no if badge")
                tabBarController?.tabBar.items?[2].badgeValue = String(describing: "1")
            }else{
                print("entou no else badge")
                tabBarController?.tabBar.items?[2].badgeValue = String(describing: "1")
            }
        }
        
    }
   
    
  
   
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        return list.count
        
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell") as! ResumoCelula
        
        cell.bloco.text = list[indexPath.row].idBloco
        cell.dataHome.text = list[indexPath.row].dataBloco
        cell.descontoHome.text = String(describing: formatCurrency(value: Double(list[indexPath.row].valorDesconto)!))
        cell.subTotalHome.text = String(describing: formatCurrency(value: Double(list[indexPath.row].valorPosAuditados)!))
        
        
        return cell
        
    }
    
}


extension HomeViewController{
    
    
    func listarBlocosAuditados(){
        
        //constante abaixo ira receber o caminho do webservice
        guard let url = URL(string: "\(servidorCompartilhado.servidor)Controller_webservice/bloco_pos_auditado") else {return}
        
        
        //variavel de requisicao ira receber o caminho da constante acima
        var request = URLRequest(url:url)
        
        //informa o tipo de envio
        request.httpMethod = "POST"
        
        
        //constante ira cricar uma string com o id que o webService espera receber e com valor ja associado
        let postString = "id_usuario=\(id!)"
        
        
        
        
        //abaixo sera feira uma associacao de tipo de codificaao de caracteres
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        
        //constante abaixo sera onde sera feita a comunicacao com o webservice
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            DispatchQueue.main.async() {
                
                //abaixo ira mostrar o estado da conexao com o webservice
                if let response = response {
                    print(response)
                }
                
                if let data = data {
                    
                    print(data)
                    
                    do{
                        
                        
                        
                        //se tudo ocorrer bem, a constante fetchedData ira receber os dados do webService
                        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! NSArray
                        print(json)
                        
                        self.list = []
                        self.totalReceber = 0.0
                        self.totalDesconto = 0.0
                        
                        if json.count == 0 {
                            
                            let alertaUsuario = UIAlertController(title: "Aviso", message: "Você ainda não tem dados para consultar.", preferredStyle: .alert)
                            let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
                            alertaUsuario.addAction(acaoFechar)
                            self.present(alertaUsuario, animated: true, completion: nil)

                            
                        }
                    
                        for eachArrayInformacoes in json{
                            
                            
                            let eachInformacoes = eachArrayInformacoes as! [String: Any]
                            let idBloco             = eachInformacoes["id_bloco"] as? String
                            let dataBloco           = eachInformacoes["data_bloco"] as? String
                            let qtdCanhotos         = eachInformacoes["qtd_canhotos"] as? String
                            let valor               = eachInformacoes["valor"] as? String
                            let auditados           = eachInformacoes["auditados"] as? String
                            let posAuditados        = eachInformacoes["pos_auditados"] as? String
                            let valorPosAuditados   = eachInformacoes["valor_pos_auditado"] as? String
                            let valorDesconto   = eachInformacoes["valor_desconto"] as? String

                            self.totalReceber = self.totalReceber + Double(valorDesconto!)!
                            self.totalDesconto = self.totalDesconto + Double(valorPosAuditados!)!
                            
                            self.list.append(PosAuditacao(idBloco: idBloco!, dataBloco: dataBloco!, qtdCanhotos: qtdCanhotos!, valor: valor!, auditados: auditados!, posAuditados: posAuditados!, valorPosAuditados: valorPosAuditados!, valorDesconto: valorDesconto!))
                            
                        }
                        
                        self.pararAnimacao()
                        self.contaBadge()
                        
                        
                        self.tabelaHome.reloadData()
                        self.descontoTotalHome.text = String(formatCurrency(value: Double(self.totalReceber)))
                        self.valorTotalHome.text    = String(formatCurrency(value: Double(self.totalDesconto)))
                    
                        
                    }catch {
                        //se nao conseguir se conectar ao webservice ira mostrar o erro aqui
                        print(error)
                        let alertaUsuario = UIAlertController(title: "Aviso", message: "Ocorreu um erro ao consultar seus blocos concluídos.", preferredStyle: .alert)
                        let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
                        alertaUsuario.addAction(acaoFechar)
                        self.present(alertaUsuario, animated: true, completion: nil)
                        self.pararAnimacao()
                    }
                }
                
            }}
        
        
        
        task.resume()
    }
    
    
}


