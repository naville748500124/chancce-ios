//
//  MudarSenhaViewController.swift
//  Chancce
//
//  Created by Marcos Barbosa on 11/09/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import UIKit

class MudarSenhaViewController: UIViewController {
    
    
    @IBOutlet weak var botaoCancelarNovaSenha: UIButton!
    @IBOutlet weak var botaoConfirmarNovaSenha: UIButton!
    @IBOutlet weak var senhaAtualTextField: UITextField!
    @IBOutlet weak var novaSenhaTextField: UITextField!
    @IBOutlet weak var confirmarSenhaTextField: UITextField!
    @IBOutlet weak var viewActivity: UIView!
    @IBOutlet weak var activityMudarSenha: UIActivityIndicatorView!
    
    //variaveis
    let acaoFechar =        UIAlertAction(title: "Fechar", style: .default, handler: nil)
    var novaSenha:          String!
    var confirmaNovaSenha:  String!
    var senha:              String!
    var servidorCompartilhado = UIApplication.shared.delegate as! AppDelegate


    override func viewDidLoad() {
        super.viewDidLoad()

        // borda dos botoes
        
        self.pararAnimacao()
        
        botaoCancelarNovaSenha.layer.cornerRadius = 8
        botaoConfirmarNovaSenha.layer.cornerRadius = 8
        
        senhaAtualTextField.layer.cornerRadius     = 5
        senhaAtualTextField.layer.borderColor      = UIColor.gray.cgColor
        senhaAtualTextField.layer.borderWidth      = 0.8
        
        confirmarSenhaTextField.layer.cornerRadius     = 5
        confirmarSenhaTextField.layer.borderColor      = UIColor.gray.cgColor
        confirmarSenhaTextField.layer.borderWidth      = 0.8
        
        novaSenhaTextField.layer.cornerRadius     = 5
        novaSenhaTextField.layer.borderColor      = UIColor.gray.cgColor
        novaSenhaTextField.layer.borderWidth      = 0.8
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func iniciarAnimacao(){
        
        self.activityMudarSenha.startAnimating()
        self.viewActivity.isHidden = false
        
    }
    
    func pararAnimacao(){
        
        self.activityMudarSenha.stopAnimating()
        self.viewActivity.isHidden = true
        
    }


    @IBAction func acaoCancelarMudarSenha(_ sender: Any) {
        
        fecharTeclado()
        apagarCampos()
        
        self.navigationController?.popViewController(animated: true)
      
        
    }
    
    
    @IBAction func senhaAtualReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func novaSenhaReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func confirmarNovaSenhaReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    
    @IBAction func acaoConfirmarMudarSenha(_ sender: Any) {
        
        fecharTeclado()
        
        if senhaAtualTextField.text == "" {
            
            let alertaUsuario = UIAlertController(title: "Atualização de Senha", message: "O campo senha atual está vazio.", preferredStyle: .alert)
            alertaUsuario.addAction(acaoFechar)
            present(alertaUsuario, animated: true, completion: nil)
            
            
        }else{
            
            senha = senhaAtualTextField.text
            
            if novaSenhaTextField.text == "" {
                
                let alertaUsuario = UIAlertController(title: "Atualização de Senha", message: "O campo nova senha está vazio.", preferredStyle: .alert)
                alertaUsuario.addAction(acaoFechar)
                present(alertaUsuario, animated: true, completion: nil)
                
            }else{
                
                self.novaSenha = novaSenhaTextField.text
                
                if self.novaSenha.characters.count < 8 {
                    
                    let alertaUsuario = UIAlertController(title: "Atualização de Senha", message: "Sua senha precisa ter no minimo 8 digitos.", preferredStyle: .alert)
                    alertaUsuario.addAction(acaoFechar)
                    present(alertaUsuario, animated: true, completion: nil)
                    
                    
                }else{
                    
                    self.novaSenha = novaSenhaTextField.text
                    
                    if confirmarSenhaTextField.text == "" {
                        
                        let alertaUsuario = UIAlertController(title: "Atualização de Senha", message: "O campo confirmar nova senha está vazio.", preferredStyle: .alert)
                        alertaUsuario.addAction(acaoFechar)
                        present(alertaUsuario, animated: true, completion: nil)
                        
                    }else{
                        
                        
                        self.confirmaNovaSenha = confirmarSenhaTextField.text
                        
                        if self.confirmaNovaSenha != self.novaSenha {
                            
                            let alertaUsuario = UIAlertController(title: "Atualização de Senha", message: "A nova senha e a confirmação de senha precisam ser iguais.", preferredStyle: .alert)
                            alertaUsuario.addAction(acaoFechar)
                            present(alertaUsuario, animated: true, completion: nil)
                            
                            
                        }else{
                            
                            self.confirmaNovaSenha = confirmarSenhaTextField.text
                            
                            if isInternetAvailable() == true{
                                
                                self.iniciarAnimacao()
                                AlterarSenhaBanco()
                                
                                
                            }else{
                                
                                let alertaUsuario = UIAlertController(title: "Atenção", message: "Falha na conexão com a internet.", preferredStyle: .alert)
                                let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
                                alertaUsuario.addAction(acaoFechar)
                                self.present(alertaUsuario, animated: true, completion: nil)
                                self.pararAnimacao()
                                
                            }
                            
                            
                        }
                    }
                }
            }
        }
    }

}

extension MudarSenhaViewController{
    
    func AlterarSenhaBanco() {
        
        
        //constante abaixo ira receber o caminho do webservice
        guard let url = URL(string: "\(servidorCompartilhado.servidor)Controller_webservice/nova_senha") else {return}

        
        //variavel de requisicao ira receber o caminho da constante acima
        var request = URLRequest(url:url)
        
        //informa o tipo de envio
        request.httpMethod = "POST"
        
        var senhaAtualCriptografada: String!
        var senhaNovaCriptografada:  String!
        
        senhaAtualCriptografada = senha.sha1()
        senhaNovaCriptografada  = novaSenha.sha1()
        
        
        //constante ira cricar uma string com o id que o webService espera receber e com valor ja associado
        let postString = "idUsuario=\(id!)&senhaAntiga=\(senhaAtualCriptografada!)&senhaNova=\(senhaNovaCriptografada!)"
        
        //abaixo sera feira uma associacao de tipo de codificaao de caracteres
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        
        //constante abaixo sera onde sera feita a comunicacao com o webservice
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            DispatchQueue.main.async() {
                
                //abaixo ira mostrar o estado da conexao com o webservice
                if let response = response {
                    print(response)
                }
                
                if let data = data {
                    
                    print(data)
                    
                    do{
                        
                        //se tudo ocorrer bem, a constante fetchedData ira receber os dados do webService
                        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! String
                        
                        
                        print(json)
                        
                        if json == "SENHA ALTERADA COM SUCESSO"{
                            
                            let alertaUsuario = UIAlertController(title: "Alterar senha", message: "Sua senha foi alterada com sucesso.", preferredStyle: .alert)
                            let acaoOk = UIAlertAction(title: "Fechar", style: .default, handler: { (action) in
                                self.navigationController?.popToRootViewController(animated: true)
                            })
                            //alertaUsuario.addAction(self.acaoFechar)
                            alertaUsuario.addAction(acaoOk)
                            self.present(alertaUsuario, animated: true, completion: nil)
                            
                            self.apagarCampos()
                            
                            self.servidorCompartilhado.defaults.set(true, forKey: "validaManterConectado")
                            self.servidorCompartilhado.defaults.set("", forKey: "senhaDefault")

                            
                        }else if json == "Erro AO ALTERAR SENHA"{
                            
                            let alertaUsuario = UIAlertController(title: "Alterar senha", message: "Ocorreu um erro e sua senha não pode ser alterada.", preferredStyle: .alert)
                            alertaUsuario.addAction(self.acaoFechar)
                            self.present(alertaUsuario, animated: true, completion: nil)
                            self.apagarCampos()
                            
                        }else if json == "SENHA INCORRETA"{
                            
                            let alertaUsuario = UIAlertController(title: "Alterar senha", message: "A senha digitada é incorreta.", preferredStyle: .alert)
                            alertaUsuario.addAction(self.acaoFechar)
                            self.present(alertaUsuario, animated: true, completion: nil)
                            self.apagarCampos()
                        }
                        
                        self.pararAnimacao()
                        
                        
                    }catch {
                        //se nao conseguir se conectar ao webservice ira mostrar o erro aqui
                        print(error)
                        
                        
                        let alertaUsuario = UIAlertController(title: "Atençao", message: "Ocorreu um erro ao alterar a senha.", preferredStyle: .alert)
                        let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
                        alertaUsuario.addAction(acaoFechar)
                        self.present(alertaUsuario, animated: true, completion: nil)
                        self.pararAnimacao()
                        
                    }
                }
                
            }}
        task.resume()
        
    }
    
    func apagarCampos() {
        
        senhaAtualTextField.text     = ""
        novaSenhaTextField.text      = ""
        confirmarSenhaTextField.text = ""
        
    }
}

//funcao de limitar caracteres
extension MudarSenhaViewController: UITextFieldDelegate {
    
    //esta funcao ira limitar o numero de caracteres nos campos
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (senhaAtualTextField.inputDelegate != nil) {
            
            return senhaAtualTextField.text!.characters.count + string.characters.count <= 16
            
        }else if (novaSenhaTextField.inputDelegate != nil){
            
            return novaSenhaTextField.text!.characters.count + string.characters.count <= 16
            
        }else if (confirmarSenhaTextField.inputDelegate != nil){
            
            return confirmarSenhaTextField.text!.characters.count + string.characters.count <= 16
        }
        
        return true
    }

    
}

//função de fechar o teclado
extension MudarSenhaViewController{
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        fecharTeclado()
        
    }
    
    func fecharTeclado(){
        
        self.view.endEditing(true)
        
    }
}
