//
//  RestaurarSenhaViewController.swift
//  Chancce
//
//  Created by Marcos Barbosa on 09/08/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import UIKit

class RestaurarSenhaViewController: UIViewController {
    
    //referencias de campos da tela
    @IBOutlet weak var usuarioTextField: UITextField!
    @IBOutlet weak var botaoRecuperar: UIButton!
    @IBOutlet weak var viewActivity: UIView!
    @IBOutlet weak var activityEsqueceuSenha: UIActivityIndicatorView!
    
    //variavel para armazenar o email
    var recuperaEmail: String!
    let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
     var servidorCompartilhado = UIApplication.shared.delegate as! AppDelegate
    

    override func viewDidLoad() {
        super.viewDidLoad()

        //customização dos componentes
        usuarioTextField.layer.cornerRadius = 8
        
        botaoRecuperar.layer.cornerRadius = 8
        self.pararAnimacao()
        
    }

    
    func iniciarAnimacao(){
        
        self.activityEsqueceuSenha.startAnimating()
        self.viewActivity.isHidden = false
        
    }
    
    func pararAnimacao(){
        
        self.activityEsqueceuSenha.stopAnimating()
        self.viewActivity.isHidden = true
        
    }
   
    
    @IBAction func acaoRecuperarSenha(_ sender: Any) {
        
        fecharTeclado()
        
        self.recuperaEmail = usuarioTextField.text
        
        if self.recuperaEmail == ""{
            
            let alertaUsuario = UIAlertController(title: "Recuperar Senha", message: "O campo email do usuario está vazio.", preferredStyle: .alert)
            alertaUsuario.addAction(acaoFechar)
            present(alertaUsuario, animated: true, completion: nil)
        
        }else{
            
            if self.recuperaEmail.isValidEmail() == false{
                
                let alertaUsuario = UIAlertController(title: "Recuperar Senha", message: "Não é um endereço de email válido.", preferredStyle: .alert)
                alertaUsuario.addAction(acaoFechar)
                present(alertaUsuario, animated: true, completion: nil)
                
            }else{
                
                if isInternetAvailable() == true{
                    
                    self.iniciarAnimacao()
                    restaurarSenha()
                    
                }else{
                    
                    let alertaUsuario = UIAlertController(title: "Atenção", message: "Falha na conexão com a internet.", preferredStyle: .alert)
                    let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
                    alertaUsuario.addAction(acaoFechar)
                    self.present(alertaUsuario, animated: true, completion: nil)
                    self.pararAnimacao()
                    
                }
                
                
                
            }
            
        }
        
    }

}

//somente conexao com o webService
extension RestaurarSenhaViewController{
    
    func restaurarSenha(){
        
        //constante abaixo ira receber o caminho do webservice
        
        guard let url = URL(string: "\(servidorCompartilhado.servidor)Controller_webservice/recuperarSenha") else {return}
        
        //variavel de requisicao ira receber o caminho da constante acima
        var request = URLRequest(url:url)
        
        //informa o tipo de envio
        request.httpMethod = "POST"
        
        
        //constante ira cricar uma string com o id que o webService espera receber e com valor ja associado
        let postString = "emailUsuario=\(self.recuperaEmail!)"
        print("idUsuario=\(self.recuperaEmail!)")
        
        //abaixo sera feira uma associacao de tipo de codificaao de caracteres
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        
        //constante abaixo sera onde sera feita a comunicacao com o webservice
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            DispatchQueue.main.async() {
                
                //abaixo ira mostrar o estado da conexao com o webservice
                if let response = response {
                    print(response)
                }
                
                if let data = data {
                    
                    print(data)
                    
                    do{
                        
                        //se tudo ocorrer bem, a constante fetchedData ira receber os dados do webService
                        let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, AnyObject>
                        print(json)
                        
                        
                        let status = json["status"] as? Int
                        
                        if status == 0{
                            
                            let alertaUsuario = UIAlertController(title: "Recuperar Senha", message: "O email enviado, não possui conta neste app.", preferredStyle: .alert)
                            alertaUsuario.addAction(self.acaoFechar)
                            self.present(alertaUsuario, animated: true, completion: nil)
                            
                            self.apagarCampo()
                            
                        }else {
                            
                            let alertaUsuario = UIAlertController(title: "Recuperar Senha", message: "Senha enviada com sucesso para o seu email.", preferredStyle: .alert)
                            alertaUsuario.addAction(self.acaoFechar)
                            self.present(alertaUsuario, animated: true, completion: nil)
                            self.apagarCampo()
                        }
                        
                        self.pararAnimacao()
                        
                    }catch {
                        //se nao conseguir se conectar ao webservice ira mostrar o erro aqui
                        print(error)
                        
                        let alertaUsuario = UIAlertController(title: "Atençao", message: "Ocorreu um erro ao recuperar a senha.", preferredStyle: .alert)
                        let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
                        alertaUsuario.addAction(acaoFechar)
                        self.present(alertaUsuario, animated: true, completion: nil)
                        self.pararAnimacao()
                    }
                }
                
            }}
        task.resume()

    }
    
    
    func apagarCampo(){
        
        usuarioTextField.text = ""
        
    }
    
}


//função que ira subir alguns campos para o teclado não ficar em cima, e funcao de fechar o teclado
extension RestaurarSenhaViewController{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        moveTextField(textField: textField, moveDistance: -215, up: true)
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        moveTextField(textField: textField, moveDistance: -215, up: false)
        
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    
    func moveTextField(textField: UITextField, moveDistance:  Int, up: Bool){
        
        let moveDuration = 0.3
        let movement: CGFloat = CGFloat(up ? moveDistance: -moveDistance)
        
        UIView.beginAnimations("animatedTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(moveDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
        
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
        
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        fecharTeclado()
        
    }

    func fecharTeclado(){
        
        self.view.endEditing(true)
        
    }
    
    //mudar cor da status bar
    override public var preferredStatusBarStyle: UIStatusBarStyle{
        
        return .lightContent
        
    }
    
}
