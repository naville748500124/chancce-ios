//
//  ContainerVizualizaCanhoto.swift
//  Chancce
//
//  Created by Marcos Barbosa on 05/09/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import UIKit

class ContainerVizualizaCanhoto: UIViewController {

    
    @IBOutlet weak var webView: UIWebView!
    @IBOutlet weak var botaoComparar: UIButton!
    @IBOutlet weak var botaoProximo: UIButton!
    @IBOutlet weak var dataCanhoto: UILabel!
    @IBOutlet weak var viewActivity: UIView!
    @IBOutlet weak var activityVizualiza: UIActivityIndicatorView!
    
    @IBOutlet weak var labelOK: UILabel!
    
    @IBOutlet weak var selectOk: UISwitch!
    @IBOutlet weak var selectCamposBrancos: UISwitch!
    @IBOutlet weak var selectCampoIlegivel: UISwitch!
    @IBOutlet weak var selectConferenciaIncompletaSPP: UISwitch!
    @IBOutlet weak var selectDataDivergente: UISwitch!
    
    var opcoes = ""
    
    var idCanhoto       : String = ""
    var imgLink         : String = ""
    var canhotoData     : String = ""
    var canhotoAtual    : String = ""
    var canhotoTotal    : String = ""
    var imgLinkProximo  : String = ""
    var exibirAviso = true
    var segundos = 4
    var tempo = Timer()
    
    var validaLink = false
    
     var servidorCompartilhado = UIApplication.shared.delegate as! AppDelegate
    
    override func viewDidLoad() {
        super.viewDidLoad()
        

        self.botaoComparar.layer.cornerRadius = 8
        self.botaoComparar.layer.borderWidth  = 2
        self.botaoProximo.layer.cornerRadius  = 8
        self.botaoProximo.isUserInteractionEnabled = false
        self.botaoProximo.setTitle("...", for: .normal)
        
        if isInternetAvailable() == true{
            
            selectOk.isEnabled = false
            selectCamposBrancos.isEnabled = false
            selectCampoIlegivel.isEnabled = false
            selectConferenciaIncompletaSPP.isEnabled = false
            selectDataDivergente.isEnabled = false
            
            self.iniciarAnimacao()
            BuscarCanhotos()
     
        }else{
            
            let alertaUsuario = UIAlertController(title: "Aviso", message: "Falha na conexão com a internet.", preferredStyle: .alert)
            let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
            alertaUsuario.addAction(acaoFechar)
            self.present(alertaUsuario, animated: true, completion: nil)
            self.pararAnimacao()
            
        }
        
        self.labelOK.isMultipleTouchEnabled = true
        if self.labelOK.isHighlighted == true {
            
            print("clicou")
        }
        
    }
    
    func atualizandoTempo(){
        
        segundos -= 1
        self.botaoProximo.setTitle("\(segundos)", for: .normal)
        self.botaoProximo.isUserInteractionEnabled = false
        
        if segundos <= 00 {
            tempo.invalidate()
            
            selectOk.isEnabled = true
            selectCamposBrancos.isEnabled = true
            selectCampoIlegivel.isEnabled = true
            selectConferenciaIncompletaSPP.isEnabled = true
            selectDataDivergente.isEnabled = true
            
            if self.canhotoAtual == self.canhotoTotal{
                self.botaoProximo.setTitle("Finalizar", for: .normal)
                self.botaoProximo.isUserInteractionEnabled = true
                segundos = 4
            } else {
                self.botaoProximo.setTitle("Próximo", for: .normal)
                self.botaoProximo.isUserInteractionEnabled = true
                segundos = 4
            }
            
            selectOk.isEnabled = true
            selectCamposBrancos.isEnabled = true
            selectCampoIlegivel.isEnabled = true
            selectConferenciaIncompletaSPP.isEnabled = true
            selectDataDivergente.isEnabled = true
            
        } else {
            selectOk.isEnabled = false
            selectCamposBrancos.isEnabled = false
            selectCampoIlegivel.isEnabled = false
            selectConferenciaIncompletaSPP.isEnabled = false
            selectDataDivergente.isEnabled = false
        }
        
    }
    
    func sair(action: UIAlertAction){
        
        self.navigationController?.popViewController(animated: true)
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func iniciarAnimacao(){
        
        self.activityVizualiza.startAnimating()
        self.viewActivity.isHidden = false
        
    }
    
    func pararAnimacao(){
        
        self.activityVizualiza.stopAnimating()
        self.viewActivity.isHidden = true
        
    }
    

    
    @IBAction func acaoMudarCAnhoto(_ sender: Any) {
        
        //self.validaLink = !self.validaLink
        if validaLink {
            
            self.iniciarAnimacao()
            self.botaoComparar.setTitle("Comparar", for: .normal)
            self.validaLink = false
            abrirWebView()
            
            
        }else{
            
            self.iniciarAnimacao()
            self.botaoComparar.setTitle("Voltar", for: .normal)
            self.validaLink = true
            abrirWebView()
        }
       
        
    }
    

    func abrirWebView(){
        
        
        //print(url)
        DispatchQueue.main.async() {
            
            //referencia o delegate
            self.webView.delegate = self as? UIWebViewDelegate
            if self.validaLink == false {
                if let url = URL(string: (self.imgLink)){
                    let request = URLRequest(url: url)
                    self.webView.loadRequest(request)
                    self.pararAnimacao()
                    
                }
            }else{
                
                if let url = URL(string: (self.imgLinkProximo)){
                    let request = URLRequest(url: url)
                    self.webView.loadRequest(request)
                    self.pararAnimacao()
                    
                }
                
            }
        }
    }
    
    
    
    
    @IBAction func switchOK(_ sender: Any) {
        
        if ((sender as AnyObject).isOn){
            
            print("on")
            selectCamposBrancos.isOn = false
            selectCamposBrancos.isEnabled = false
            
            selectCampoIlegivel.isOn = false
            selectCampoIlegivel.isEnabled = false
            
            selectConferenciaIncompletaSPP.isOn = false
            selectConferenciaIncompletaSPP.isEnabled = false
            
            selectDataDivergente.isOn = false
            selectDataDivergente.isEnabled = false
            
        }else{
            print("off")
            
            selectCamposBrancos.isEnabled = true
            selectCampoIlegivel.isEnabled = true
            selectConferenciaIncompletaSPP.isEnabled = true
            selectDataDivergente.isEnabled = true
            
        }
        
    }
    
    @IBAction func switchCampoBranco(_ sender: Any) {
        
        if ((sender as AnyObject).isOn){
        
            selectOk.isOn = false
        }
        
    }
    
    @IBAction func switchEnderecoDistinto(_ sender: Any) {
        
        if ((sender as AnyObject).isOn){
            
            selectOk.isOn = false
        }
        
    }
    
    @IBAction func switchConferenciaIncompleta(_ sender: Any) {
        
        if ((sender as AnyObject).isOn){
            
            selectOk.isOn = false
        }
        
    }
    
    @IBAction func switchConferenciaIncompletaSPP(_ sender: Any) {
        
        if ((sender as AnyObject).isOn){
            
            selectOk.isOn = false
        }
        
    }
    
    @IBAction func switchDataDivergente(_ sender: Any) {
        
        if ((sender as AnyObject).isOn){
            
            selectOk.isOn = false
        }
        
    }
    
    @IBAction func switchCamposMotorista(_ sender: Any) {
        
        if ((sender as AnyObject).isOn){
            
            selectOk.isOn = false
        }
        
    }
    
    @IBAction func switchImagemCortada(_ sender: Any) {
        
        if ((sender as AnyObject).isOn){
            
            selectOk.isOn = false
        }
        
    }
    
    @IBAction func switchErroRG(_ sender: Any) {
        
        if ((sender as AnyObject).isOn){
            
            selectOk.isOn = false
        }
        
    }
    
    @IBAction func switchCanhotoCarimbo(_ sender: Any) {
        
        if ((sender as AnyObject).isOn){
            
            selectOk.isOn = false
        }
        
    }
    
    //executa a acao de buscar e imprimir o proximo canhoto, e verificar se algo foi auditado
    @IBAction func acaoBotaoProximo(_ sender: Any) {
        
        if  (selectCamposBrancos.isOn == false && selectCampoIlegivel.isOn == false && selectConferenciaIncompletaSPP.isOn == false &&
             selectDataDivergente.isOn == false && selectOk.isOn == false){
            
                let alertaUsuario = UIAlertController(title: "Aviso", message: "Nenhum tipo de auditoria foi selecionado.", preferredStyle: .alert)
                let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
                alertaUsuario.addAction(acaoFechar)
                present(alertaUsuario, animated: true, completion: nil)
 
        }else{
            
            selectOk.isEnabled = false
            selectCamposBrancos.isEnabled = false
            selectCampoIlegivel.isEnabled = false
            selectConferenciaIncompletaSPP.isEnabled = false
            selectDataDivergente.isEnabled = false
            
            self.iniciarAnimacao()
            self.enviarWs()
        
        }
        
    }
    
    func enviarWs(){
        
        var opcoesMarcadas = ""
        
        if selectOk.isOn {
            opcoes += "3,"
            opcoesMarcadas += "● O canhoto não apresenta nenhum tipo de erro"
        }
        
        if selectCamposBrancos.isOn {
            opcoes += "4,"
            opcoesMarcadas += "● O Canhoto possui algum campo não preenchido \n"
        }
        if selectCampoIlegivel.isOn {
            opcoes += "5,"
            opcoesMarcadas += "● Contem as mesmas informações em endereços distintos \n"
        }
        if selectConferenciaIncompletaSPP.isOn  {
            opcoes += "7,"
            opcoesMarcadas += "● CONFERÊNCIA OBRIGATÓRIA INCOMPLETA (SPP) \n"
        }
        if selectDataDivergente.isOn {
            opcoes += "6,"
            opcoesMarcadas += "● A data que está no canhoto é diferente do que consta no sistema. \n"
        }
        
        if  exibirAviso {
            
            let refreshAlert = UIAlertController(title: "Confirmação", message: "Antes de enviar confirme o que foi marcado: \n \(opcoesMarcadas)", preferredStyle: UIAlertControllerStyle.alert)
            
            refreshAlert.addAction(UIAlertAction(title: "Confirmar", style: .default, handler: { (action: UIAlertAction!) in
                self.envioConfirmado()
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Confirmar e interromper aviso", style: .default, handler: { (action: UIAlertAction!) in
                self.envioConfirmado()
                self.exibirAviso = false
            }))
            
            refreshAlert.addAction(UIAlertAction(title: "Cancelar", style: .cancel, handler: { (action: UIAlertAction!) in
                self.pararAnimacao()
            }))
            
            present(refreshAlert, animated: true, completion: nil)
            
        } else {
            self.envioConfirmado()
        }
        
        
    }
    
    func envioConfirmado(){
        let id_canhoto = self.idCanhoto
        
        selectOk.isEnabled = false
        selectCamposBrancos.isEnabled = false
        selectCampoIlegivel.isEnabled = false
        selectConferenciaIncompletaSPP.isEnabled = false
        selectDataDivergente.isEnabled = false
        
        //constante abaixo ira receber o caminho do webservice
        guard let url = URL(string: "\(servidorCompartilhado.servidor)Controller_webservice/atualizar_Canhoto") else {return}
        
        //variavel de requisicao ira receber o caminho da constante acima
        var request = URLRequest(url:url)
        
        //informa o tipo de envio
        request.httpMethod = "POST"
        
        //constante ira cricar uma string com o id que o webService espera receber e com valor ja associado
        let postString = "ws_id_canhoto=\(id_canhoto)&fk_opcao_checklist=\(self.opcoes)&id_usuario=\(id!)&id_bloco=\(blocoEscolhido!)"
        
        print("ws_id_canhoto=\(id_canhoto)&fk_opcao_checklist=\(self.opcoes)&id_usuario=\(id!)&id_bloco=\(blocoEscolhido!)")
        
        //abaixo sera feira uma associacao de tipo de codificaao de caracteres
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        
        //constante abaixo sera onde sera feita a comunicacao com o webservice
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            DispatchQueue.main.async() {
                
                //abaixo ira mostrar o estado da conexao com o webservice
                if let response = response {
                    print(response)
                }
                
                if let data = data {
                    
                    print(data)
                    
                    do{
                        
                        
                        
                        if self.canhotoAtual == self.canhotoTotal {
                            
                            
                            let alertaUsuario = UIAlertController(title: "Parabéns", message: "Auditoria dos canhotos concluida com sucesso.", preferredStyle: .alert)
                            let pegar       = UIAlertAction(title: "Fechar", style: .default, handler: self.sair)
                            alertaUsuario.addAction(pegar)
                            self.present(alertaUsuario, animated: true, completion: nil)
                            
                            if Int(badgeUser)! > 0 {
                                let badget = Int(badgeUser)! - 1
                                badgeUser = String(badget)
                            }
                            
                            
                            
                        }else{
                            //FEED BACK DE ENVIO
                            
                            
                            let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, Any>
                            print(json)
                            
                            
                            
                            print("passou do json 0")
                            
                            self.idCanhoto          = (json["id_canhoto"] as? String)!
                            self.imgLink            = (json["img_link"] as? String)!
                            self.canhotoData        = (json["data_canhoto"] as? String)!
                            self.canhotoAtual       = (json["canhoto_atual"] as? String)!
                            self.canhotoTotal       = (json["canhoto_total"] as? String)!
                            self.imgLinkProximo     = (json["img_link_proximo"] as? String)!
                            
                            self.title = "\(self.canhotoAtual)/\(self.canhotoTotal)"
                            self.dataCanhoto.text = "Data do canhoto \(self.canhotoData)"
                            
                            
                            self.tempo = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(OportunidadesEmAbertoCell.atualizandoTempo)), userInfo: nil, repeats: true)
                            
                            self.validaLink = false
                            self.abrirWebView()
                            self.deselect()
                            
                            self.pararAnimacao()
                            
                        }
                        
                    }catch {
                        //se nao conseguir se conectar ao webservice ira mostrar o erro aqui
                        
                        func sair(action: UIAlertAction){
                            
                            
                            self.navigationController?.popToRootViewController(animated: true)
                            
                            
                        }
                        
                        
                        let alertaUsuario = UIAlertController(title: "Aviso", message: "Tempo de auditoria esgotado.", preferredStyle: .alert)
                        let pegar       = UIAlertAction(title: "Fechar", style: .default, handler: sair)
                        alertaUsuario.addAction(pegar)
                        self.present(alertaUsuario, animated: true, completion: nil)
                        
                        
                        print(error)
                        
                        
                    }
                }
                
                
                
                
            }}
        task.resume()
    }
    
    
    
    func retornoParaListaCanhotos(){
        
        if abreCanhoto == true{
            
            print("entrou aqui na segue de retorno")
            performSegue(withIdentifier: "retornoCanhoto", sender: self)
            
            
        }
        
    }
    
    func deselect(){
        
        selectOk.isOn = false
        selectCamposBrancos.isOn = false
        selectCampoIlegivel.isOn = false
        selectConferenciaIncompletaSPP.isOn = false
        selectDataDivergente.isOn = false
       
        selectCamposBrancos.isEnabled = true
        selectCampoIlegivel.isEnabled = true
        selectConferenciaIncompletaSPP.isEnabled = true
        selectDataDivergente.isEnabled = true
        
        self.opcoes = ""
        
    }
    
}

extension ContainerVizualizaCanhoto{
    
    func BuscarCanhotos(){
        
       
        
        //constante abaixo ira receber o caminho do webservice
        guard let url = URL(string: "\(servidorCompartilhado.servidor)Controller_webservice/ler_Canhotos") else {return}
        
        
        
        print("printando url")
        print(url)
        //variavel de requisicao ira receber o caminho da constante acima
        var request = URLRequest(url:url)
        
        //informa o tipo de envio
        request.httpMethod = "POST"
        
        
        //constante ira cricar uma string com o id que o webService espera receber e com valor ja associado
        let postString = "id_bloco=\(blocoEscolhido!)"
        print("printando bloco selecionao\(blocoEscolhido)")
        
        
        //abaixo sera feira uma associacao de tipo de codificaao de caracteres
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        
        //constante abaixo sera onde sera feita a comunicacao com o webservice
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            DispatchQueue.main.async() {
                
                //abaixo ira mostrar o estado da conexao com o webservice
                if let response = response {
                    print(response)
                }
                
                if let data = data {
                    
                    print(data)
                    
                    do{
                        
                        let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, Any>
                        print(json)
                        
                        
                            self.idCanhoto          = (json["id_canhoto"] as? String)!
                            self.imgLink            = (json["img_link"] as? String)!
                            self.canhotoData        = (json["data_canhoto"] as? String)!
                            self.canhotoAtual       = (json["canhoto_atual"] as? String)!
                            self.canhotoTotal       = (json["canhoto_total"] as? String)!
                            self.imgLinkProximo     = (json["img_link_proximo"] as? String)!
                        
                        self.title = "\(self.canhotoAtual)/\(self.canhotoTotal)"
                        self.dataCanhoto.text = "Data do canhoto \(self.canhotoData)"
                        
                        self.tempo = Timer.scheduledTimer(timeInterval: 1, target: self, selector: (#selector(OportunidadesEmAbertoCell.atualizandoTempo)), userInfo: nil, repeats: true)
                        
                        self.abrirWebView()
                        self.pararAnimacao()
                    }catch {
                        //se nao conseguir se conectar ao webservice ira mostrar o erro aqui
                        print(error)
                        let alertaUsuario = UIAlertController(title: "Aviso", message: "Algo deu errado ao carregar.", preferredStyle: .alert)
                        let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: self.sair)
                        alertaUsuario.addAction(acaoFechar)
                        self.present(alertaUsuario, animated: true, completion: nil)
                        self.pararAnimacao()
                       
                        
                    }
                }
                
            }}
        task.resume()
        
        
    }
    
   
    
}
