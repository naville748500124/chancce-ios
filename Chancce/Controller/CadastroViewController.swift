//
//  TesteController.swift
//  Chancce
//
//  Created by Marcos on 04/07/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import Foundation
import UIKit
import CryptoSwift

class CadastroViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UITextFieldDelegate{
    
    //referencia dos campos da tela
    @IBOutlet weak var nomeTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var senhaTextField: UITextField!
    @IBOutlet weak var telefoneTextField: UITextField!
    @IBOutlet weak var bancoTextField: UITextField!
    @IBOutlet weak var agenciaTextField: UITextField!
    @IBOutlet weak var contaTextField: UITextField!
    @IBOutlet weak var digitoTextField: UITextField!
    @IBOutlet weak var cpfTextField: UITextField!
    @IBOutlet weak var botaoFinalizar: UIButton!
    @IBOutlet weak var imagemPerfil: UIImageView!
    @IBOutlet weak var botaoAdicionarFoto: UIButton!
    @IBOutlet weak var viewBotoesFoto: UIView!
    @IBOutlet weak var termosUso: UISwitch!
    @IBOutlet weak var viewDaActivity: UIView!
    @IBOutlet weak var activityCadastro: UIActivityIndicatorView!
    
    @IBOutlet weak var dataNascimentoTextField: UIDatePicker!
    
    var idade = 0
    
    //variaveis
    let acaoFechar = UIAlertAction(title: "Fechar", style: .default, handler: nil)
    var nome: String!
    var nomeCaixaBaixa: String!
    var celular: String!
    var CPF: String!
    var email: String!
    var senha: String!
    var emailRecuperado: String!
    var validadorEmail: Bool!
    var banco: String!
    var agencia: String!
    var conta: String!
    var digito: String!
    var senhaCriptografada: String!
    var validaCadastro: Int!
    var mensagem = false
    var checou   = false
    var stringIbagem: UIImage!
    var bancoPicker = UIPickerView()
    var bancoEscolhido: String!
    var fixaTeclado = false
    var servidorCompartilhado = UIApplication.shared.delegate as! AppDelegate
    
    //cria o array de usuarios
    var arrayInformacoes = [Dados]()
    var arrayBancos      = [Bancos]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if servidorCompartilhado.id_facebook != "" {
            
            nomeTextField.text = servidorCompartilhado.nome_facebook
            emailTextField.text = servidorCompartilhado.email_facebook
            
            let url = URL(string:"http://graph.facebook.com/\(servidorCompartilhado.id_facebook)/picture?type=large")!
            
            let dado = try? Data(contentsOf: url)
            let imagem: UIImage = UIImage(data: dado!)!

            imagemPerfil.image = imagem
            
        }
        
        print("Foto comum")
        let image : UIImage = UIImage(named: "forma1Copiar16.png")!
        let imageView = UIImageView(frame: CGRect(x: 100, y: 100, width: 30, height: 32))
        imageView.contentMode = .scaleAspectFit
        imageView.image = image
        self.navigationItem.titleView = imageView
        
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "< Voltar", style: .plain, target: self, action: nil)
        self.navigationItem.rightBarButtonItem?.tintColor = UIColor.clear
        
        dataNascimentoTextField.addTarget(self, action: #selector(dateChanged(_:)), for: .valueChanged)
        
        //popula picker banco
        BuscarBancos()
        bancoPicker.delegate = self
        bancoPicker.dataSource = self
        bancoPicker.tag = 1
        bancoTextField.inputView = bancoPicker
        
        //borda dos botoes
        botaoFinalizar.layer.cornerRadius = 8
        
        
        //customizaçao dos campos
        nomeTextField.layer.cornerRadius = 5
        nomeTextField.layer.borderWidth = 0.8
        nomeTextField.layer.borderColor = UIColor.gray.cgColor
        
        emailTextField.layer.cornerRadius = 5
        emailTextField.layer.borderWidth = 0.8
        emailTextField.layer.borderColor = UIColor.gray.cgColor
        
        senhaTextField.layer.cornerRadius = 5
        senhaTextField.layer.borderWidth = 0.8
        senhaTextField.layer.borderColor = UIColor.gray.cgColor
        
        telefoneTextField.layer.cornerRadius = 5
        telefoneTextField.layer.borderWidth = 0.8
        telefoneTextField.layer.borderColor = UIColor.gray.cgColor
        
        
        bancoTextField.layer.cornerRadius   = 5
        bancoTextField.layer.borderColor    = UIColor.gray.cgColor
        bancoTextField.layer.borderWidth    = 0.8
        
        agenciaTextField.layer.cornerRadius = 5
        agenciaTextField.layer.borderColor  = UIColor.gray.cgColor
        agenciaTextField.layer.borderWidth  = 0.8
        
        contaTextField.layer.cornerRadius   = 5
        contaTextField.layer.borderColor    = UIColor.gray.cgColor
        contaTextField.layer.borderWidth    = 0.8
        
        cpfTextField.layer.cornerRadius     = 5
        cpfTextField.layer.borderColor      = UIColor.gray.cgColor
        cpfTextField.layer.borderWidth      = 0.8
        
        digitoTextField.layer.cornerRadius     = 5
        digitoTextField.layer.borderColor      = UIColor.gray.cgColor
        digitoTextField.layer.borderWidth      = 0.8
        
        
        //arredondar a foto do foto escolhida
        imagemPerfil?.layer.cornerRadius = imagemPerfil.frame.size.width/2
        imagemPerfil?.clipsToBounds = true
        
        //view de escolher galeria ou camera
        viewBotoesFoto.isHidden = true
        viewBotoesFoto.layer.cornerRadius = 5
        
        //Looks for single or multiple taps.
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(dismisss))
        
        //Uncomment the line below if you want the tap not not interfere and cancel other interactions.
        //tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
   
    
        pararAnimacao()
        
        if isInternetAvailable() == true{
            
            iniciaAnimacao()
            selecionarBanco()
            
        }else{
            
            let alertaUsuario = UIAlertController(title: "Aviso", message: "Falha na conexão com a internet.", preferredStyle: .alert)
            alertaUsuario.addAction(self.acaoFechar)
            self.present(alertaUsuario, animated: true, completion: nil)
            pararAnimacao()
            
        }
        
    }
    
    @objc func dismisss()
    {
        view.endEditing(true)
    }
    
    @objc func dateChanged(_ sender: UIDatePicker) {
        let componenets = Calendar.current.dateComponents([.year, .month, .day], from: sender.date)
        if let day = componenets.day, let month = componenets.month, let year = componenets.year {
            print("\(day) \(month) \(year)")
            
            let date = Date()
            let formatter = DateFormatter()
            
            formatter.dateFormat = "yyyy-MM-dd"
            
            let start = "\(year)-\(month)-\(day)"
            let end = formatter.string(from: date)
            
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyy-MM-dd"
            
            let startDate:Date = dateFormatter.date(from: start)! as Date
            let endDate:Date = dateFormatter.date(from: end)! as Date
            
            idade = daysBetweenDates(startDate: startDate as Date,endDate: endDate as Date)
            
        }
    }
    
    func daysBetweenDates(startDate: Date, endDate: Date) -> Int {
        let calendar = Calendar.current
        let components = calendar.dateComponents([Calendar.Component.year], from: startDate, to: endDate)
        return components.year!
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func nomeTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func emailTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func senhaTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func telefoneTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func bancoTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func agenciaTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func contaTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func cpfTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func digitoTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func botaoCameraOuGaleria(_ sender: Any) {
        
        if self.viewBotoesFoto.isHidden == true{
            
            self.viewBotoesFoto.isHidden = false
            
        }else{
            
            self.viewBotoesFoto.isHidden = true
            
        }
        
        
    }
    
    
    //função do botao que vai abrir a camera
    @IBAction func abrirCamera(_ sender: Any) {
        
        let image = UIImagePickerController()
        image.delegate = self
        
        image.sourceType = UIImagePickerControllerSourceType.camera
        
        
        image.allowsEditing = true
        
        
        
        self.present(image, animated: true){
            
        }
        self.viewBotoesFoto.isHidden = true
        
        
    }
    
    
    //funcao do botao que vai abrir a galeria
    @IBAction func abrirGaleria(_ sender: Any) {
        
        fecharTeclado()
        
        let image = UIImagePickerController()
        image.delegate = self
        
        image.sourceType = UIImagePickerControllerSourceType.photoLibrary
        
        
        image.allowsEditing = true
        
        
        
        self.present(image, animated: true){
            
        }
        self.viewBotoesFoto.isHidden = true
        
    }
    
    //esta funcao ira limitar o numero de caracteres nos campos
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        if (agenciaTextField.inputDelegate != nil) {
            
            return agenciaTextField.text!.characters.count + string.characters.count <= 8
            
        }else if (contaTextField.inputDelegate != nil){
            
            return contaTextField.text!.characters.count + string.characters.count <= 16
            
        }else if (nomeTextField.inputDelegate != nil){
            
            return nomeTextField.text!.characters.count + string.characters.count <= 96
            
        }else if (emailTextField.inputDelegate != nil){
            
            return emailTextField.text!.characters.count + string.characters.count <= 64
            
        }else if (digitoTextField.inputDelegate != nil){
            
            return digitoTextField.text!.characters.count + string.characters.count <= 1
            
        }else if (senhaTextField.inputDelegate != nil){
            
            return senhaTextField.text!.characters.count + string.characters.count <= 16
            
        }
        
        
        return true
    }
    
    
    @IBAction func acaoTermos(_ sender: Any) {
        
        self.performSegue(withIdentifier: "irTermos", sender: nil)
        fecharTeclado()
        
    }
    
    //funcao do botao finalizar, que irá verificar os dados nos campos e ira validar, se estiver algo errado imprimira uma mensagem, caso contrario irá cadastrar no banco
    @IBAction func acaoBotaoFinalizar(_ sender: Any) {
        
        
        fecharTeclado()
        CPF = cpfTextField.text
        emailRecuperado = emailTextField.text
        
        if nomeTextField.text == "" {
            
            let alertaUsuario = UIAlertController(title: "Aviso", message: "O campo nome está vazio.", preferredStyle: .alert)
            alertaUsuario.addAction(acaoFechar)
            present(alertaUsuario, animated: true, completion: nil)
            
            
        }else{
            
            self.nome = nomeTextField.text
            self.nomeCaixaBaixa = nome.lowercased()
            
            if emailTextField.text == ""{
                
                let alertaUsuario = UIAlertController(title: "Aviso", message: "O campo e-mail está vazio.", preferredStyle: .alert)
                alertaUsuario.addAction(acaoFechar)
                present(alertaUsuario, animated: true, completion: nil)
                
            }else{
                
                validaEmail()
                
                if validadorEmail ==  false{
                    
                    let alertaUsuario = UIAlertController(title: "Aviso", message: "O e-mail digitado não é valido.", preferredStyle: .alert)
                    alertaUsuario.addAction(acaoFechar)
                    present(alertaUsuario, animated: true, completion: nil)
                    
                }else{
                    
                    self.emailRecuperado = emailTextField.text
                    
                    if senhaTextField.text == ""{
                        
                        let alertaUsuario = UIAlertController(title: "Aviso", message: "O campo senha está vazio.", preferredStyle: .alert)
                        alertaUsuario.addAction(acaoFechar)
                        present(alertaUsuario, animated: true, completion: nil)
                        
                    }else{
                        
                        self.senha = senhaTextField.text
                        
                        
                        
                        if senha.characters.count < 8 {
                            print("menor que 8")
                            
                            let alertaUsuario = UIAlertController(title: "Aviso", message: "Sua senha precisa ter no mínimo 8 digitos.", preferredStyle: .alert)
                            alertaUsuario.addAction(acaoFechar)
                            present(alertaUsuario, animated: true, completion: nil)
                            
                        }else{
                            
                            self.senha = senhaTextField.text
                            
                            if telefoneTextField.text! == "" || (telefoneTextField.text?.characters.count)! < 15{
                                
                                let alertaUsuario = UIAlertController(title: "Aviso", message: "O campo celular está vazio, ou está incorreto.", preferredStyle: .alert)
                                alertaUsuario.addAction(acaoFechar)
                                present(alertaUsuario, animated: true, completion: nil)
                                
                            }else{
                                
                                self.celular = telefoneTextField.text
                                
                                if bancoTextField.text == "" || bancoTextField.text == "Selecione..."{
                                    
                                    let alertaUsuario = UIAlertController(title: "Aviso", message: "O campo banco está vazio.", preferredStyle: .alert)
                                    alertaUsuario.addAction(acaoFechar)
                                    present(alertaUsuario, animated: true, completion: nil)
                                    
                                }else{
                                    self.banco = self.bancoEscolhido
                                    
                                    if agenciaTextField.text == "" {
                                        
                                        let alertaUsuario = UIAlertController(title: "Aviso", message: "O campo agência está vazio.", preferredStyle: .alert)
                                        alertaUsuario.addAction(acaoFechar)
                                        present(alertaUsuario, animated: true, completion: nil)
                                        
                                    }else{
                                        self.agencia = agenciaTextField.text
                                        
                                        if contaTextField.text! == "" || digitoTextField.text == ""{
                                            
                                            let alertaUsuario = UIAlertController(title: "Aviso", message: "O campo conta ou digito está vazio.", preferredStyle: .alert)
                                            alertaUsuario.addAction(acaoFechar)
                                            present(alertaUsuario, animated: true, completion: nil)
                                            
                                        }else{
                                            self.conta = contaTextField.text
                                            self.digito = digitoTextField.text
                                            
                                            if cpfTextField.text == ""{
                                                
                                                let alertaUsuario = UIAlertController(title: "Aviso", message: "O campo CPF está vazio.", preferredStyle: .alert)
                                                alertaUsuario.addAction(acaoFechar)
                                                present(alertaUsuario, animated: true, completion: nil)
                                                
                                            }else{
                                                
                                                if CPF.isValidoCPF == false {
                                                    let alertaUsuario = UIAlertController(title: "Aviso", message: "O CPF digitado nao é valido.", preferredStyle: .alert)
                                                    alertaUsuario.addAction(acaoFechar)
                                                    present(alertaUsuario, animated: true, completion: nil)
                                                    
                                                    
                                                }else{
                                                    
                                                    print("else do cpf")
                                                    
                                                    self.CPF = cpfTextField.text
                                                    
                                                    if imagemPerfil.image == nil {
                                                        
                                                        print("if da imagem")
                                                        imagemPerfil.image = #imageLiteral(resourceName: "imagem_usuario")
                                                        
                                                        
                                                        if termosUso.isOn == false{
                                                            print("if dos termos")
                                                            let alertaUsuario = UIAlertController(title: "Aviso", message: "Você precisa aceitar os termos de uso para poder se cadastrar.", preferredStyle: .alert)
                                                            alertaUsuario.addAction(acaoFechar)
                                                            present(alertaUsuario, animated: true, completion: nil)
                                                            
                                                        }else{
                                                           
                                                            if  idade > 17 {
                                                                if isInternetAvailable() == true{
                                                                    
                                                                    
                                                                    ChecarUsuario()
                                                                    
                                                                }else{
                                                                    
                                                                    let alertaUsuario = UIAlertController(title: "Aviso", message: "Falha na conexão com a internet.", preferredStyle: .alert)
                                                                    alertaUsuario.addAction(self.acaoFechar)
                                                                    self.present(alertaUsuario, animated: true, completion: nil)
                                                                    
                                                                    self.activityCadastro.stopAnimating()
                                                                    self.viewDaActivity.isHidden = true
                                                                    
                                                                }
                                                            } else {
                                                                
                                                                let alertaUsuario = UIAlertController(title: "Aviso", message: "É Necessário ter mais de 18 anos", preferredStyle: .alert)
                                                                alertaUsuario.addAction(self.acaoFechar)
                                                                self.present(alertaUsuario, animated: true, completion: nil)
                                                                
                                                                self.activityCadastro.stopAnimating()
                                                                self.viewDaActivity.isHidden = true
                                                                
                                                            }
                                                            
                                                            
                                                            
                                                        }

                                                        
                                                    }else{
                                                        
                                                        if termosUso.isOn == false{
                                                            print("if dos termos")
                                                            let alertaUsuario = UIAlertController(title: "Aviso", message: "Você precisa aceitar os termos de uso para poder se cadastrar.", preferredStyle: .alert)
                                                            alertaUsuario.addAction(acaoFechar)
                                                            present(alertaUsuario, animated: true, completion: nil)
                                                            
                                                        }else{
                                                            if idade > 17 {
                                                                
                                                                if isInternetAvailable() == true{
                                                                    
                                                                    iniciaAnimacao()
                                                                    ChecarUsuario()
                                                                    
                                                                }else{
                                                                    
                                                                    let alertaUsuario = UIAlertController(title: "Aviso", message: "Falha na conexão com a internet.", preferredStyle: .alert)
                                                                    alertaUsuario.addAction(self.acaoFechar)
                                                                    self.present(alertaUsuario, animated: true, completion: nil)
                                                                    
                                                                    pararAnimacao()
                                                                    
                                                                    
                                                                }
                                                                
                                                            } else {
                                                                
                                                                let alertaUsuario = UIAlertController(title: "Aviso", message: "É Necessário ter mais de 18 anos", preferredStyle: .alert)
                                                                alertaUsuario.addAction(self.acaoFechar)
                                                                self.present(alertaUsuario, animated: true, completion: nil)
                                                                
                                                                self.activityCadastro.stopAnimating()
                                                                self.viewDaActivity.isHidden = true
                                                                
                                                            }
                                                            
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
}

//funcao de popular o picker
extension CadastroViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView( _ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return arrayBancos.count
    }
    
    func pickerView( _ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        
        return arrayBancos[row].nomeBanco
        
    }
    
    func pickerView( _ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        self.bancoEscolhido = String(arrayBancos[row].idBanco)
        bancoTextField.text = arrayBancos[row].nomeBanco
    }
    
    func BuscarBancos(){
        
        arrayBancos = []
        
        //constante abaixo ira receber o caminho do webservice
        guard let url = URL(string: "\(servidorCompartilhado.servidor)Controller_webservice/listar_Bancos") else {return}
        
        

        print("printando url")
        print(url)
        //variavel de requisicao ira receber o caminho da constante acima
        var request = URLRequest(url:url)
        
        //informa o tipo de envio
        request.httpMethod = "POST"
        
        
        //constante ira cricar uma string com o id que o webService espera receber e com valor ja associado
        let postString = ""
        
        
        //abaixo sera feira uma associacao de tipo de codificaao de caracteres
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        
        //constante abaixo sera onde sera feita a comunicacao com o webservice
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            DispatchQueue.main.async() {
                
                //abaixo ira mostrar o estado da conexao com o webservice
                if let response = response {
                    print(response)
                }
                
                if let data = data {
                    
                    print(data)
                    
                    do{
                        
                        let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as! NSArray
                        print(json)
                        
                        self.arrayBancos.append(Bancos(idBanco: 0, nomeBanco: "Selecione..."))
                        
                        
                        for eachArrayInformacoes in json{
                            
                            let eachInformacoes = eachArrayInformacoes as! [String: Any]
                            let idBanco = eachInformacoes["idBanco"] as? String
                            let nomeBanco = eachInformacoes["nomeBanco"] as? String
                            
                            self.arrayBancos.append(Bancos(idBanco: Int(idBanco!)!, nomeBanco: nomeBanco!))
                            
                        }
                        
                        self.pararAnimacao()
                        
                       self.bancoTextField.text = self.arrayBancos[0].nomeBanco
                        
                    }catch {
                        //se nao conseguir se conectar ao webservice ira mostrar o erro aqui
                        print(error)
                        
                    }
                }
                
            }}
        task.resume()
        
        
    }
    
    func selecionarBanco(){
        
        let tooBar = UIToolbar()
        tooBar.barStyle = UIBarStyle.default
        tooBar.isTranslucent = true
        tooBar.tintColor = UIColor(red: 255/255, green: 255/255, blue: 255/255, alpha: 1)
        tooBar.barTintColor = UIColor(red: 0/255, green: 11/255, blue: 36/255, alpha: 1)
        tooBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Selecionar", style: UIBarButtonItemStyle.plain, target: self, action: #selector(doneBanco))
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancelar", style: UIBarButtonItemStyle.plain, target: self, action: #selector(cancelarPicker))
        
        tooBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        tooBar.isUserInteractionEnabled = true
        
        bancoTextField.inputAccessoryView = tooBar
        
    }
    
    //essa funcao escolhe uma banco
    @objc func doneBanco(){
        
        if(self.banco == nil){
            bancoTextField.resignFirstResponder()
            print("esta aqui \(self.bancoEscolhido)")
        }else{
            //bancoTextField.text = self.bancoEscolhido
            bancoTextField.resignFirstResponder()
            print("agora esta aqui \(self.bancoEscolhido)")
            selecionarBanco()
            
        }
    }
    
    //essa funcao cancela o escolhimento de uma midia
    @objc func cancelarPicker(){
        
        bancoTextField.resignFirstResponder()
        
    }
    
}

//função que ira recuperar a imagem e atribui-la a variavel imagemPerfil
extension CadastroViewController{
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let image = info[UIImagePickerControllerEditedImage] as? UIImage{
            
            imagemPerfil.image = image
            
        }else{
            
            print("deu erro na imagem ")
            
        }
        
        self.dismiss(animated: true, completion: nil)
        
    }
    
}

//função que ira subir alguns campos para o teclado não ficar em cima
//o fixaTeclado ira receber um valor boleano, para na fucao de terminar de editar validar se a tela fica fixa ou se recua os 215 que subiu quando comecou a editar
extension CadastroViewController{
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        
        if nomeTextField.isEditing == true || emailTextField.isEditing ==  true {
            
            moveTextField(textField: textField, moveDistance: 0, up: false)
            self.fixaTeclado = true
            
        }else {
            
            moveTextField(textField: textField, moveDistance: -215, up: true)
            self.fixaTeclado = false
            
        }
        
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
        
        if self.fixaTeclado == true{
            
            moveTextField(textField: textField, moveDistance: 0, up: false)
            
        }else{
            
            moveTextField(textField: textField, moveDistance: -215, up: false)
            
        }
    }
    
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        
        textField.resignFirstResponder()
        return true
        
    }
    
    
    func moveTextField(textField: UITextField, moveDistance:  Int, up: Bool){
        
        let moveDuration = 0.3
        let movement: CGFloat = CGFloat(up ? moveDistance: -moveDistance)
        
        UIView.beginAnimations("animatedTextField", context: nil)
        UIView.setAnimationBeginsFromCurrentState(true)
        UIView.setAnimationDuration(moveDuration)
        self.view.frame = self.view.frame.offsetBy(dx: 0, dy: movement)
        UIView.commitAnimations()
        
    }
    
    func dismissKeyboard() {
        //Causes the view (or one of its embedded text fields) to resign the first responder status.
        view.endEditing(true)
        
    }
    
}

//função de fechar o teclado
extension CadastroViewController{
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        fecharTeclado()
        
    }
    
    func fecharTeclado(){
        
        self.view.endEditing(true)
        
    }
}

//função que ira verificar o email
extension CadastroViewController{
    
    func validaEmail(){
        
        if emailRecuperado.isValidoEmail(){
            
            self.validadorEmail = true
            
        }else{
            
            self.validadorEmail = false
            
        }
        
    }
    
    func validaNome(){
        
    }
    
}


//função de validar email e validar cpf
extension String {
    var isValidoCPF: Bool {
        let numbers = characters.flatMap({Int(String($0))})
        guard numbers.count == 11 && Set(numbers).count != 1 else { return false }
        let soma1 = 11 - ( numbers[0] * 10 +
            numbers[1] * 9 +
            numbers[2] * 8 +
            numbers[3] * 7 +
            numbers[4] * 6 +
            numbers[5] * 5 +
            numbers[6] * 4 +
            numbers[7] * 3 +
            numbers[8] * 2 ) % 11
        let dv1 = soma1 > 9 ? 0 : soma1
        let soma2 = 11 - ( numbers[0] * 11 +
            numbers[1] * 10 +
            numbers[2] * 9 +
            numbers[3] * 8 +
            numbers[4] * 7 +
            numbers[5] * 6 +
            numbers[6] * 5 +
            numbers[7] * 4 +
            numbers[8] * 3 +
            numbers[9] * 2 ) % 11
        let dv2 = soma2 > 9 ? 0 : soma2
        return dv1 == numbers[9] && dv2 == numbers[10]
    }
    
    func isValidoEmail() -> Bool {
        
        let regex = try! NSRegularExpression(pattern: "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$", options: .caseInsensitive)
        return regex.firstMatch(in: self, options: [], range: NSRange(location: 0, length: characters.count)) != nil
        
    }
    
}

extension CadastroViewController{
    
    func ChecarUsuario(){
        
        //constante abaixo ira receber o caminho do webservice
        guard let url = URL(string: "\(servidorCompartilhado.servidor)Controller_webservice/checarUsuarioExistente") else {return}

        
        //variavel de requisicao ira receber o caminho da constante acima
        var request = URLRequest(url:url)
        
        //informa o tipo de envio
        request.httpMethod = "POST"
        
        self.senhaCriptografada = self.senha.md5()
        
        //constante ira cricar uma string com o id que o webService espera receber e com valor ja associado
        let postString = "emailUsuario=\(self.emailRecuperado!)"
        
        
        //abaixo sera feira uma associacao de tipo de codificaao de caracteres
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        
        //constante abaixo sera onde sera feita a comunicacao com o webservice
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            DispatchQueue.main.async() {
                
                //abaixo ira mostrar o estado da conexao com o webservice
                if let response = response {
                    print(response)
                }
                
                if let data = data {
                    
                    print(data)
                    
                    do{
                        
                        //se tudo ocorrer bem, a constante fetchedData ira receber os dados do webService
                        let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, AnyObject>
                        print(json)
                        
                        self.validaCadastro = json["success"] as! Int
                        
                        
                        if self.validaCadastro == 1 {
                            
                            let alertaUsuario = UIAlertController(title: "Aviso", message: "E-mail já cadastrado no banco", preferredStyle: .alert)
                            alertaUsuario.addAction(self.acaoFechar)
                            self.present(alertaUsuario, animated: true, completion: nil)
                            self.pararAnimacao()
                            
                        }else{
                            
                           
                            self.CadastrarUsuario()
                            
                        }
                        
                       
                        
                    }catch {
                        //se nao conseguir se conectar ao webservice ira mostrar o erro aqui
                        print(error)
                        
                    }
                }
                
            }}
        task.resume()
    }
    
    
    
    func CadastrarUsuario(){
        
        //constante abaixo ira receber o caminho do webservice
        guard let url = URL(string: "\(servidorCompartilhado.servidor)Controller_webservice/novo_Usuario") else {return}

        
        //variavel de requisicao ira receber o caminho da constante acima
        var request = URLRequest(url:url)
        
        //informa o tipo de envio
        request.httpMethod = "POST"
        
        self.senhaCriptografada = self.senha.sha1()
        
        
        
        //Use image name from bundle to create NSData
        let image : UIImage = imagemPerfil.image!
        
        
        
        //Now use image to create into NSData format
        //let imageData:NSData = UIImagePNGRepresentation(image)! as NSData
        //let imageData:NSData = UIImageJPEGRepresentation(image, 1.0)! as NSData
        
        let imageData = image.lowestQualityJPEGNSData
        let strBase64 = imageData.base64EncodedString(options: .lineLength64Characters)
        
        //constante ira cricar uma string com o id que o webService espera receber e com valor ja associado
        let postString = "nomeUsuario=\(self.nomeCaixaBaixa!)&nomeUsuarioVisualizacao=\(self.nome!)&senhaUsuario=\(self.senhaCriptografada!)&emailUsuario=\(self.emailRecuperado!)&telefoneUsuario=\(self.celular!)&bancoUsuario=\(self.banco!)&agenciaUsuario=\(self.agencia!)&contaUsuario=\(self.conta!)&cpfUsuario=\(self.CPF!)&imagemUsuario=\(strBase64)&digitoUsuario=\(self.digito!)&id_facebook=\(self.servidorCompartilhado.id_facebook)&link_facebook=\(self.servidorCompartilhado.link_facebook)"
        
//        print(self.nomeCaixaBaixa)
//        print(self.nome)
//        print(self.senha)
//        print(self.senhaCriptografada)
//        print(self.emailRecuperado)
//        print(self.celular)
//        print(self.banco)
//        print(self.agencia)
//        print(self.conta)
//        print(self.CPF)
//        print(strBase64)
//        
        //abaixo sera feira uma associacao de tipo de codificaao de caracteres
        request.httpBody = postString.data(using: String.Encoding.utf8)
        
        
        //constante abaixo sera onde sera feita a comunicacao com o webservice
        let task = URLSession.shared.dataTask(with: request) { (data: Data?, response: URLResponse?, error: Error?) in
            
            DispatchQueue.main.async() {
                
                //abaixo ira mostrar o estado da conexao com o webservice
                if let response = response {
                    print(response)
                }
                
                if let data = data {
                    
                    print(data)
                    
                    do{
                        
                        //se tudo ocorrer bem, a constante fetchedData ira receber os dados do webService
                        let json = try JSONSerialization.jsonObject(with: data, options: JSONSerialization.ReadingOptions.mutableContainers) as! Dictionary<String, AnyObject>
                        
                        
                        print(json)
                        
                        self.validaCadastro = json["success"] as! Int
                        print(self.validaCadastro)
                        
                        if self.validaCadastro == 1 {
                            
                            self.mensagem = true
                            
                            let alertaUsuario = UIAlertController(title: "Aviso", message: "Seu cadastro foi concluído com sucesso.", preferredStyle: .alert)
                            let voltarLogin = UIAlertAction(title: "Fechar", style: .default, handler: { (action) in
                                self.navigationController?.popToRootViewController(animated: true)
                            })
                            alertaUsuario.addAction(voltarLogin)
                            self.present(alertaUsuario, animated: true, completion: nil)
                           
                            
                        }else{
                            
                            
                            let alertaUsuario = UIAlertController(title: "Aviso", message: json["message"] as? String , preferredStyle: .alert)
                            alertaUsuario.addAction(self.acaoFechar)
                            self.present(alertaUsuario, animated: true, completion: nil)

                            
                        }
                        
                        self.pararAnimacao()
                        
                    }catch {
                        //se nao conseguir se conectar ao webservice ira mostrar o erro aqui
                        print(error)
                        
                    }
                }
                
            }}
        task.resume()
        
    }
    
}

//fucao que ira compactar a imagem para enviar para webservice com poucos bytes

extension UIImage
{
    var highestQualityJPEGNSData: NSData { return UIImageJPEGRepresentation(self, 1.0)! as NSData }
    var highQualityJPEGNSData: NSData    { return UIImageJPEGRepresentation(self, 0.75)! as NSData}
    var mediumQualityJPEGNSData: NSData  { return UIImageJPEGRepresentation(self, 0.5)! as NSData }
    var lowQualityJPEGNSData: NSData     { return UIImageJPEGRepresentation(self, 0.25)! as NSData}
    var lowestQualityJPEGNSData: NSData  { return UIImageJPEGRepresentation(self, 0.0)! as NSData }
}

extension CadastroViewController{
    
   
    
    func iniciaAnimacao(){
        
        self.activityCadastro.startAnimating()
        self.viewDaActivity.isHidden = false
        
    }
    
    func pararAnimacao() {
        
        self.activityCadastro.stopAnimating()
        self.viewDaActivity.isHidden = true
        
    }
    
    
}


//classe Dados
class Dados{
    
    var idUser:         String
    var nomeUser:       String
    var emailUser:      String
    var telefoneUser:   String
    var bancoUser:      String
    var agenciaUser:    String
    var contaUser:      String
    var cpfUser:        String
    var imagemUser:     UIImage
    
    
    init(idUser: String,nomeUser: String,emailUser: String, telefoneUser: String, bancoUser: String, agenciaUser: String, contaUser: String, cpfUser: String, imagemUser: UIImage){
        
        self.idUser         = idUser
        self.nomeUser       = nomeUser
        self.emailUser      = emailUser
        self.telefoneUser   = telefoneUser
        self.bancoUser      = bancoUser
        self.agenciaUser    = agenciaUser
        self.contaUser      = contaUser
        self.cpfUser        = cpfUser
        self.imagemUser     = imagemUser
        
    }
    
}

class Bancos {
    
    var idBanco: Int
    var nomeBanco: String
    
    init(idBanco: Int, nomeBanco: String){
        
        self.idBanco = idBanco
        self.nomeBanco = nomeBanco
        
    }
    
}



