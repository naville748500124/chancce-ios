//
//  ContainerPerfilController.swift
//  Chancce
//
//  Created by Marcos Barbosa on 08/08/17.
//  Copyright © 2017 n/a. All rights reserved.
//

import UIKit

class ContainerPerfilController: UIViewController {

    //referencia dos campos da tela
    @IBOutlet weak var nomeTextField: UITextField!
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var senhaTextField: UITextField!
    @IBOutlet weak var telefoneTextField: UITextField!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()

    
        //customizaçao dos campos
        nomeTextField.layer.cornerRadius = 5
        nomeTextField.layer.borderWidth = 0.8
        nomeTextField.layer.borderColor = UIColor.gray.cgColor
        nomeTextField.isEnabled = true
        
        emailTextField.layer.cornerRadius = 5
        emailTextField.layer.borderWidth = 0.8
        emailTextField.layer.borderColor = UIColor.gray.cgColor
        
        senhaTextField.layer.cornerRadius = 5
        senhaTextField.layer.borderWidth = 0.8
        senhaTextField.layer.borderColor = UIColor.gray.cgColor
        
        telefoneTextField.layer.cornerRadius = 5
        telefoneTextField.layer.borderWidth = 0.8
        telefoneTextField.layer.borderColor = UIColor.gray.cgColor
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    
    @IBAction func nomeTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func emailTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func senhaTextField(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
    @IBAction func telefoneTextFieldReturn(_ sender: Any) {
        
        fecharTeclado()
        
    }
    
}

extension ContainerPerfilController{
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        
        fecharTeclado()
        
    }
    
    func fecharTeclado(){
        
        self.view.endEditing(true)
        
    }
    
}
